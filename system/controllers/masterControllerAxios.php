<?php
session_start();
require_once '../databases/connections.php';
ini_set("display_errors", false);
// ini_set('memory_limit', '1024M');
date_default_timezone_set('America/Santiago');
$output = new stdClass();

$data_ = json_decode(file_get_contents('php://input'), true);

if (!isset($_SESSION["user_id"]) || is_null($_SESSION["user_id"])) {
    echo json_encode("Session Error");
    header("location: ../views/login/");
}

switch ($data_["option"]) {
    case 'get vehicle models':
        $qry_vehicle_model = "SELECT id, name FROM vehicle_models WHERE vehicle_brand_id = ? ORDER BY name;";
        $stmt_vehicle_model = $connection->prepare($qry_vehicle_model);
        $stmt_vehicle_model->bind_param("i", $data_["vehicleBrandId"]);
        $stmt_vehicle_model->execute();
        $result_vehicle_model = $stmt_vehicle_model->get_result();
        $vehicle_model_selector = '<option value="" selected>Seleccione</option>';

        foreach ($result_vehicle_model->fetch_all(MYSQLI_ASSOC) as $a => $data) {
            $vehicle_model_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
        }

        $stmt_vehicle_model->close();

        echo $vehicle_model_selector;
        break;

    case 'get vehicles by customer id':
        $qry = 
            "SELECT vb.name as vehicle_brand, vm.name as vehicle_model, v.vehicle_year, v.color, v.vehicle_registration
            FROM customers c
            JOIN vehicles v
                ON c.id = v.customer_id
            JOIN vehicle_models vm
                ON v.vehicle_model_id = vm.id
            JOIN vehicle_brands vb
                ON vm.vehicle_brand_id = vb.id
            WHERE c.id = ?;";
        $stmt = $connection->prepare($qry);
        $stmt->bind_param("i", $data_["customerId"]);
        $stmt->execute();
        $result = $stmt->get_result();
        $html_ = '<table class="table table-hover mt-3">
            <thead>
                <th>#</th>
                <th>MARCA</th>
                <th>MODELO</th>
                <th>AÑO</th>
                <th>COLOR</th>
                <th>PATENTE</th>
            </thead>
            <tbody>';

        if ($result->num_rows == 0) {
            $html_ .= '<tr><td colspan="6">No existen vehiculos para el cliente seleccionado</td></tr>';
        } else {
            $counter = 0;
            while ($data = $result->fetch_object()) {
                $counter++;
                $html_ .= 
                    '<tr>
                        <td>'.$counter.'</td>
                        <td>'.$data->vehicle_brand.'</td>
                        <td>'.$data->vehicle_model.'</td>
                        <td>'.$data->vehicle_year.'</td>
                        <td>'.$data->color.'</td>
                        <td>'.$data->vehicle_registration.'</td>
                    </tr>';
            }
        }
        
        $html_ .= '</tbody></table>';

        echo $html_;
        break;
    
    case 'find vehicle by identification':
        $qry = 
            "SELECT v.vehicle_registration, vm.name as model, vb.name as brand, v.vehicle_year, c.name as customername
            FROM vehicles v 
            JOIN vehicle_models vm
                ON v.vehicle_model_id = vm.id
            JOIN vehicle_brands vb
                ON vm.vehicle_brand_id = vb.id
            JOIN customers c
                ON v.customer_id = c.id
            WHERE v.vehicle_registration = ?
            GROUP BY v.id;";
        $stmt = $connection->prepare($qry);
        $stmt->bind_param("s", $data_["vehicleRegistrationNumber"]);
        $stmt->execute();
        $result = $stmt->get_result();
        $html_ = '';

        if ($result->num_rows == 0) {
            $html_ = 'Debe crear el vehiculo y/o cliente. Crear <a style="color: black;" href="../customers/">aquí</a>';
            $status_code = 0;
            $customername = "";
        } else {
            $data = $result->fetch_object();
            $html_ .= $data->brand. ' ' .$data->model. '. Año: ' .$data->vehicle_year;
            $status_code = 1;
            $customername = $data->customername;
        }

        $output->html_ = $html_;
        $output->status_code = $status_code;
        $output->customername = $customername;
        
        echo json_encode($output);
        break;
    
    default:
            
        echo json_encode("default");
        break;
}
