<?php
session_start();
require_once '../databases/connections.php';
ini_set("display_errors", false);
// ini_set('memory_limit', '1024M');
date_default_timezone_set('America/Santiago');
$output = new stdClass();

if (!isset($_SESSION["user_id"]) || is_null($_SESSION["user_id"])) {
    echo json_encode("Session Error");
    header("location: ../views/login/");
}

switch ($_POST["option"]) {
    case 'save_customer':
        // $qry_1 = "INSERT INTO addresses (address, city_id) VALUES (?, ?);";
        // $stmt_1 = $connection->prepare($qry_1);
        // $stmt_1->bind_param("si", $_POST["customer-address"], $_POST["customer-address-city"]);
        // $stmt_1->execute();
        $last_id = 1;// $connection->insert_id;
        // $stmt_1->close();
        
        $qry_2 = "INSERT INTO customers (name, address_id, phone, email) VALUES (?, ?, ?, ?);";
        $stmt_2 = $connection->prepare($qry_2);
        $stmt_2->bind_param("siss", $_POST["customer-name"], $last_id, $_POST["customer-phone"], $_POST["customer-email"]);
        $stmt_2->execute();
        $lastCustomerId = $connection->insert_id;
        $stmt_2->close();

        $qry_3 = 
            "INSERT INTO vehicles 
                (vehicle_model_id, vehicle_year, color, vehicle_type_id, customer_id, vehicle_registration, kilometer_accounting) 
                VALUES 
                (?, ?, ?, ?, ?, ?, ?);";
        $stmt_3 = $connection->prepare($qry_3);
        $stmt_3->bind_param("issiiss", 
            $_POST["vehicle-model"],
            $_POST["vehicle-year"],
            $_POST["vehicle-color"],
            $_POST["vehicle-type"], 
            $lastCustomerId, 
            $_POST["vehicle-identification"], 
            $_POST["vehicle-kilometer"]);
        $stmt_3->execute();
        $stmt_3->close();

        echo "Cliente guardado correctamente";
        echo '<script>setTimeout(function(){ window.location.href = "../views/customers/"; }, 2000);</script>';
        break;

    case 'add vehicle':
        $qry_3 = 
            "INSERT INTO vehicles 
                (vehicle_model_id, vehicle_year, color, vehicle_type_id, customer_id, vehicle_registration, kilometer_accounting) 
                VALUES 
                (?, ?, ?, ?, ?, ?, ?);";
        $stmt_3 = $connection->prepare($qry_3);
        $stmt_3->bind_param("issiiss", 
            $_POST["vehicle-model-basicform"],
            $_POST["vehicle-year-basicform"],
            $_POST["vehicle-color-basicform"],
            $_POST["vehicle-type-basicform"], 
            $_POST["customerIdForVehicle-basicform"],
            $_POST["vehicle-identification-basicform"], 
            $_POST["vehicle-kilometer-basicform"]);
        $stmt_3->execute();
        $stmt_3->close();

        echo "Cliente guardado correctamente";
        echo '<script>setTimeout(function(){ window.location.href = "../views/customers/"; }, 2000);</script>';
        break;
    
    case 'add ot':
        $vehicleId = getVehicleIdByIdentification($_POST["vehicle-identification"], $connection);
        
        if ($vehicleId > 0) {
            $qry = 
                "INSERT INTO working_orders 
                    (vehicle_id, user_id, problem_description, working_observations) 
                    VALUES 
                    (?, ?, ?, ?) ;";
            $stmt = $connection->prepare($qry);
            $stmt->bind_param("iiss", 
                $vehicleId,
                $_POST["user-id"],
                $_POST["service-problem-detail"],
                $_POST["service-solution-detail"]);
            $stmt->execute();
            $stmt->close();

            echo "Ot asignada a vehiculo: ". $vehicleId;
            echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
        } else {
            echo "Ot no creada. Intente nuevamente. ";
            echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
        }
        
        break;

    case 'add ot':
        $vehicleId = getVehicleIdByIdentification($_POST["vehicle-identification"], $connection);
        
        if ($vehicleId > 0) {
            $qry = 
                "INSERT INTO working_orders 
                    (vehicle_id, user_id, problem_description, working_observations) 
                    VALUES 
                    (?, ?, ?, ?) ;";
            $stmt = $connection->prepare($qry);
            $stmt->bind_param("iiss", 
                $vehicleId,
                $_POST["user-id"],
                $_POST["service-problem-detail"],
                $_POST["service-solution-detail"]);
            $stmt->execute();
            $stmt->close();

            echo "Ot asignada a vehiculo: ". $vehicleId;
            echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
        } else {
            echo "Ot no creada. Intente nuevamente. ";
            echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
        }
        
        break;

    case 'add service':
        if ($_POST["ot_id"] > 0) {
            try {
                $qry = 
                    "INSERT INTO working_orders_services 
                        (working_order_id, service_id, quantity, price) 
                        VALUES 
                        (?, ?, ?, ?) ;";
                $stmt = $connection->prepare($qry);
                $stmt->bind_param("iiii", 
                    $_POST["ot_id"],
                    $_POST["service-id"],
                    $_POST["service-quantity"],
                    $_POST["service-price"]);
                $stmt->execute();
                $stmt->close();

                echo "Servicio asignada a OT: ". $vehicleId;
                echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
            } catch (\Throwable $th) {
                echo "Servicio no creado. Intente nuevamente. ";
                echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
            }
            
        } else {
            echo "Servicio no creado. Intente nuevamente. ";
            echo '<script>setTimeout(function(){ window.location.href = "../views/working-orders/"; }, 2000);</script>';
        }
        
        break;
    
    default:
            
        echo json_encode("Error");
        echo '<script>setTimeout(function(){ window.location.href = "../"; }, 2000);</script>';
        break;
}

function getVehicleIdByIdentification($identification, $connection) {
    $qry = "SELECT v.id FROM vehicles v WHERE v.vehicle_registration = ? GROUP BY v.id;";
    $stmt = $connection->prepare($qry);
    $stmt->bind_param("s", $identification);
    $stmt->execute();
    $result = $stmt->get_result();
    $html_ = '';

    if ($result->num_rows == 0) {
        return 0;
    } else {
        $data = $result->fetch_object();

        return $data->id;
    }
}