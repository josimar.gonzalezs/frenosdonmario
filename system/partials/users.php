<?php include("../../helpers/usersV2.php"); ?>
<!-- Main Content -->
<div class="container mt-5 text-center">
    <h1>Usuarios</h1>
    <p>Listado de usuarios</p>
    <div class="card text-bg-light mt-<?=$esDispositivoMovil?3:5?> ms-<?=$esDispositivoMovil?3:5?>" style="width: 9<?=$esDispositivoMovil?2:5?>%;">
        <div class="card-header"><h5 class="card-title">Listado</h5></div>
        <div class="card-body">
            <?=$html?>
        </div>
    </div>
</div>

