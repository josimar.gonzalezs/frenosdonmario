<?php include("../../helpers/inventory.php"); ?>
<!-- Main Content -->
<div class="container mt-5 text-center">
    <h1>Inventario</h1>
    <p>Listado de inventario</p>
    <div class="card text-bg-light mt-<?=$esDispositivoMovil?3:5?> ms-<?=$esDispositivoMovil?3:5?>" style="width: 9<?=$esDispositivoMovil?2:5?>%;">
        <div class="card-header"><h5 class="card-title">Listado</h5></div>
        <div class="card-body">
            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i class="bi bi-plus-square"></i> Crear item iventario</button>
            <?=$html?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Ingreso Inventario</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form methot="POST" action="./">
                <div class="modal-body">
                
                    <div class="mb-3">
                        <label for="vehicle-identification" class="form-label">Patente Vehículo</label>
                        <input type="text" class="form-control" id="vehicle-identification" name="vehicle-identification">
                        <button type="button" class="btn btn-dark btn-sm">Buscar</button>
                    </div>
                    <div class="mb-3">
                        <label for="customer-name" class="form-label">Nombre Cliente</label>
                        <input type="text" class="form-control" id="customer-name" name="customer-name">
                        <button type="button" class="btn btn-dark btn-sm">Buscar</button>
                    </div>
                    <div class="mb-3">
                        <label for="service-name" class="form-label">Servicio Realizado</label>
                        <input type="text" class="form-control" id="service-name" name="service-name">
                    </div>
                    <div class="mb-3">
                        <label for="service-detail" class="form-label">Detalle Servicio</label>
                        <textarea type="text" class="form-control" id="service-detail" name="service-detail"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="price-service" class="form-label">Precio Servicio</label>
                        <input type="number" class="form-control" id="price-service" name="price-service">
                    </div>
                    <div class="mb-3">
                        <label for="user-id" class="form-label">Atendido por</label>
                        <select type="text" class="form-control" id="user-id" name="user-id">
                            <?php include "../../helpers/users.php"; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Cerrar OT</button>
                </div>
            </form>
        </div>
    </div>
</div>


