<?php include("../../helpers/working-orders.php"); ?>
<?php include("../../helpers/services.php"); ?>
<!-- Main Content -->
<div class="container mt-5 text-center">
    <h1>Órdenes de trabajo</h1>
    <p>Listado de órdenes de trabajo</p>
    <div class="card text-bg-light mt-<?=$esDispositivoMovil?3:5?> ms-<?=$esDispositivoMovil?3:5?>" style="width: 9<?=$esDispositivoMovil?2:5?>%;">
        <div class="card-header"><h5 class="card-title">Listado</h5></div>
        <div class="card-body">
            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i class="bi bi-plus-square"></i> Crear orden</button>
            <?=$html?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Ingreso OT</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="../../controllers/masterController.php">
                <div class="modal-body">
                
                    <div class="mb-3">
                        <label for="vehicle-identification" class="form-label">Patente Vehículo</label>
                        <input type="text" class="form-control" id="vehicle-identification" name="vehicle-identification" onchange="findVehicle();" required>
                        <small id="small_searching_vehicle"></small>
                    </div>
                    <div class="mb-3">
                        <label for="customer-name" class="form-label">Nombre Cliente</label>
                        <input type="text" class="form-control" id="customer-name" name="customer-name" readonly>
                    </div>
                    <div class="mb-3">
                        <label for="service-problem-detail" class="form-label">Descripción problema</label>
                        <textarea type="text" class="form-control" id="service-problem-detail" name="service-problem-detail" required></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="service-solution-detail" class="form-label">Detalle Servicio realizado</label>
                        <textarea type="text" class="form-control" id="service-solution-detail" name="service-solution-detail" required></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="user-id" class="form-label">Atendido principalmente por</label>
                        <select type="text" class="form-control" id="user-id" name="user-id" required>
                            <?php include "../../helpers/users.php"; ?>
                        </select>
                    </div>
                    <input type="hidden" id="option" name="option" required value="add ot">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Cerrar OT</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="AddService" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="AddServiceLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="AddServiceLabel">Agregar servicio</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="../../controllers/masterController.php">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="service-id" class="form-label">Servicio</label>
                        <select type="text" class="form-control" id="service-id" name="service-id" required><?=$services_selector?></select>
                    </div>
                    <div class="mb-3">
                        <label for="service-quantity" class="form-label">Cantidad</label>
                        <input type="number" class="form-control" id="service-quantity" name="service-quantity" value="" required>
                    </div>
                    <div class="mb-3">
                        <label for="service-price" class="form-label">Price</label>
                        <input type="number" class="form-control" id="service-price" name="service-price" value="" required>
                    </div>
                    <input type="hidden" id="option" name="option" required value="add service">
                    <input type="hidden" id="ot_id" name="ot_id" required value="0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Ingresar Servicio</button>
                </div>
            </form>
        </div>
    </div>
</div>


