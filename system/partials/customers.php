<?php include("../../helpers/customers.php"); ?>
<?php include("../../helpers/vehicles.php"); ?>
<!-- Main Content -->
<div class="container mt-5 text-center">
    <h1>Clientes</h1>
    <p>Listado de clientes</p>
    <div class="card text-bg-light mt-<?=$esDispositivoMovil?3:5?> ms-<?=$esDispositivoMovil?3:5?>" style="width: 9<?=$esDispositivoMovil?2:5?>%;">
        <div class="card-header"><h5 class="card-title">Listado</h5></div>
        <div class="card-body">
            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i class="bi bi-plus-square"></i> Crear Cliente</button>
            <?=$html?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Ingreso Cliente</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="../../controllers/masterController.php">
                <div class="modal-body">
                
                    <div class="mb-3">
                        <label for="customer-name" class="form-label">Nombre Cliente</label>
                        <input type="text" class="form-control" id="customer-name" name="customer-name" required>
                    </div>
                    <div class="mb-3">
                        <label for="customer-phone" class="form-label">Teléfono</label>
                        <input type="text" class="form-control" id="customer-phone" name="customer-phone" required>
                    </div>
                    <div class="mb-3">
                        <label for="customer-email" class="form-label">Correo</label>
                        <input type="text" class="form-control" id="customer-email" name="customer-email" required>
                    </div>
                    <h1 class="modal-title fs-5">Ingreso Vehículo</h1>
                    <hr></hr>
                    <div class="mb-3">
                        <label for="vehicle-brand" class="form-label">Marca</label>
                        <select type="text" class="form-control" id="vehicle-brand" name="vehicle-brand" onchange="getVehicleModels();" required><?=$vehicle_brand_selector?></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-model" class="form-label">Modelo</label>
                        <select type="text" class="form-control" id="vehicle-model" name="vehicle-model" required></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-year" class="form-label">Año</label>
                        <input type="text" class="form-control" id="vehicle-year" name="vehicle-year" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-type" class="form-label">Tipo</label>
                        <select type="text" class="form-control" id="vehicle-type" name="vehicle-type" required><?=$vehicle_type_selector?></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-color" class="form-label">Color</label>
                        <input type="text" class="form-control" id="vehicle-color" name="vehicle-color" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-identification" class="form-label">Patente</label>
                        <input type="text" class="form-control" id="vehicle-identification" name="vehicle-identification" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-kilometer" class="form-label">KM</label>
                        <input type="text" class="form-control" id="vehicle-kilometer" name="vehicle-kilometer" required>
                    </div>
                    <input type="hidden" id="option" name="option" required value="save_customer">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Guardar Cliente</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="GetVehicle" data-bs-keyboard="false" tabindex="-1" aria-labelledby="GetVehicleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="GetVehicleLabel">Vehiculos</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="getVehiclesBodyModal"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="AddVehicle" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="AddVehicleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content body-modal">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="AddVehicleLabel">Ingreso Vehiculo</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="../../controllers/masterController.php">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="vehicle-brand-basicform" class="form-label">Marca</label>
                        <select type="text" class="form-control" id="vehicle-brand-basicform" name="vehicle-brand-basicform" onchange="getVehicleModelsBasicForm();" required><?=$vehicle_brand_selector?></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-model-basicform" class="form-label">Modelo</label>
                        <select type="text" class="form-control" id="vehicle-model-basicform" name="vehicle-model-basicform" required></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-year-basicform" class="form-label">Año</label>
                        <input type="text" class="form-control" id="vehicle-year-basicform" name="vehicle-year-basicform" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-type-basicform" class="form-label">Tipo</label>
                        <select type="text" class="form-control" id="vehicle-type-basicform" name="vehicle-type-basicform" required><?=$vehicle_type_selector?></select>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-color-basicform" class="form-label">Color</label>
                        <input type="text" class="form-control" id="vehicle-color-basicform" name="vehicle-color-basicform" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-identification-basicform" class="form-label">Patente</label>
                        <input type="text" class="form-control" id="vehicle-identification-basicform" name="vehicle-identification-basicform" required>
                    </div>
                    <div class="mb-3">
                        <label for="vehicle-kilometer-basicform" class="form-label">KM</label>
                        <input type="text" class="form-control" id="vehicle-kilometer-basicform" name="vehicle-kilometer-basicform" required>
                    </div>
                    <input type="hidden" id="option" name="option" required value="add vehicle">
                    <input type="hidden" id="customerIdForVehicle-basicform" name="customerIdForVehicle-basicform" value="0" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Guardar Vehiculo</button>
                </div>
            </form>
        </div>
    </div>
</div>
