<?php require_once("../../helpers/session-valid-views.php"); ?>
<?php require_once '../../databases/connections.php'; ?>
<?php require_once("../../helpers/is-mobile.php"); ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=$init_url?>../../images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=$init_url?>../../images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=$init_url?>../../images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=$init_url?>../../images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=$init_url?>../../images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=$init_url?>../../images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=$init_url?>../../images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=$init_url?>../../images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$init_url?>../../images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=$init_url?>../../images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$init_url?>../../images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=$init_url?>../../images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$init_url?>../../images/icons/favicon-16x16.png">
    <link rel="manifest" href="<?=$init_url?>../../images/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=$init_url?>../../images/icons/ms-icon-144x144.png">
    <title>Intranet Frenos Don Mario - Ordenes trabajo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="<?=$init_url?>css/system_style.css?id=<?=rand()?>" rel="stylesheet">
</head>
<body>
    <?php require("../../partials/menu-views.php"); ?>
    <?php require("../../partials/working-orders.php"); ?>
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../../js/axios.min.js"></script>
    <script src="../../js/functions.js?ver=<?=rand()?>"></script>
</body>
</html>
