<?php 
$GLOBALS["environment"] = $_SERVER["HTTP_HOST"] ==  "frenosdonmario.cl"?"prod":"local";

if ($GLOBALS["environment"] == 'local') {
    $hostname_app = 'localhost/others/gitlab_projects/frenosdonmario/system/';
    $protocol_app = 'http';
    $init_url = $protocol_app.'://'.$hostname_app;
} else {
    $hostname_app = 'frenosdonmario.cl/system/';
    $protocol_app = 'https';
    $init_url = $protocol_app.'://'.$hostname_app;
}

class Database {
    // Variable estática para almacenar la instancia única de la clase
    private static $instance;
    private $connection;

    // Constructor privado para evitar la creación de instancias fuera de la clase
    private function __construct() {
        if ($GLOBALS["environment"] == "prod") {
            $hostname_db = 'localhost';
            $db_user = 'develcod_mechanical_workshop_user';
            $db_pass = 'P(&1Is!iP$=*';
            $db_name = "develcod_mechanical_workshop";
        } else {
            $hostname_db = 'mysql';
            $db_user = 'root';
            $db_pass = 'test';
            $db_name = "develcod_mechanical_workshop";
        }

        $this->connection = new mysqli($hostname_db, $db_user, $db_pass, $db_name);

        // Manejar errores de conexión si es necesario
        if ($this->connection->connect_error) {
            die("Error de conexión: " . $this->connection->connect_error);
        } else {
            $this->connection->set_charset("utf8mb4");
        }
    }

    // Método estático para obtener la instancia única de la clase
    public static function getInstance() {
        // Si la instancia no existe, crear una nueva
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // Método para obtener la conexión a la base de datos
    public function getConnection() {
        return $this->connection;
    }
}

// Uso del singleton para obtener la conexión a la base de datos
$db = Database::getInstance();
$connection = $db->getConnection();
