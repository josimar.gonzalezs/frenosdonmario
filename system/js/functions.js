function setWorkplaceTitle(title_id = 1) {
    let title = "";
    let workplace_title = "";

    switch (title_id) {
        case 1:
            title = "Dashboard";
            break;
        case 2:
            title = "Órdenes de trabajo (OT)";
            break;
        case 3:
            title = "Clientes";
            break;
        case 4:
            title = "Sucursales";
            break;
        case 5:
            title = "Inventario";
            break;
        case 6:
            title = "Usuarios";
            break;

        default:
            title = "Dashboard";
            break;
    }

    workplace_title = title;
    document.querySelector("#workplace_title").innerHTML = workplace_title;
}

const getComments = async (order, sku, id) => {
    const internalNotes = document.querySelector("#internalNotes");
    internalNotes.innerHTML = "";
    var data = {
        action: "comment",
        order: order,
        sku: sku,
        id: id
    }
    try {
        const response = await axios.post("dispatch-back.php", {
            params: data
        }).then((result) => result.data);
        response.data.map(comment => {
            if (comment.internal_notes || comment.ProNota) {
                comment.internal_notes ? comment.internal_notes : "";
                comment.ProNota ? comment.ProNota : "";
                internalNotes.innerHTML = `${comment.internal_notes} ${comment.ProNota} `;
            }
        })
    } catch (error) {
        console.log(error);
    }

    var modal = document.querySelector("#modal-comments");
    modal.setAttribute("data-id", id);
    modalComments = new bootstrap.Modal(modal);
    modalComments.show();
}

const findVehicle = async () => {
    document.querySelector("#small_searching_vehicle").innerHTML = "searching";
    const vehicleRegistrationNumber = document.querySelector("#vehicle-identification").value;

    var data = {
        option: "find vehicle by identification",
        vehicleRegistrationNumber: vehicleRegistrationNumber
    }
    
    try {
        const response = await axios.post("../../controllers/masterControllerAxios.php", data);
        document.querySelector("#small_searching_vehicle").innerHTML = response.data.html_;

        if (response.data.status_code == 0) {
            document.querySelector("#vehicle-identification").value = "";
            document.querySelector("#customer-name").value = "";
        } else {
            document.querySelector("#customer-name").value = response.data.customername;
        }
    } catch (error) {
        document.querySelector("#small_searching_vehicle").innerHTML = "";
        document.querySelector("#vehicle-identification").value = "";
        document.querySelector("#customer-name").value = "";
    }
}

const setCustomerIdForVehicle = async (id) => {
    document.querySelector("#customerIdForVehicle-basicform").value = id;
}

const getVehicleModels = async () => {
    const vehicleBrandId = document.querySelector("#vehicle-brand").value;
    
    var data = {
        option: "get vehicle models",
        vehicleBrandId: vehicleBrandId
    }
    
    try {
        const response = await axios.post("../../controllers/masterControllerAxios.php", data);
        document.querySelector("#vehicle-model").innerHTML = response.data;
    } catch (error) {
        console.log(error);
    }
}

const getVehicleModelsBasicForm = async () => {
    const vehicleBrandId = document.querySelector("#vehicle-brand-basicform").value;
    
    var data = {
        option: "get vehicle models",
        vehicleBrandId: vehicleBrandId
    }
    
    try {
        const response = await axios.post("../../controllers/masterControllerAxios.php", data);
        document.querySelector("#vehicle-model-basicform").innerHTML = response.data;
    } catch (error) {
        console.log(error);
    }
}

const setCustomerIdForVehicleGet = async (customerId) => {
    var data = {
        option: "get vehicles by customer id",
        customerId: customerId
    }
    
    try {
        const response = await axios.post("../../controllers/masterControllerAxios.php", data);
        document.querySelector("#getVehiclesBodyModal").innerHTML = response.data;
        var modal = document.querySelector("#GetVehicle");
        modal.setAttribute("getVehiclesBodyModal", response);
        modalGetVehicles = new bootstrap.Modal(modal);
        modalGetVehicles.show();
    } catch (error) {
        console.log(error);
    }
}

const setWorkingIdForServices = async (id) => {
    document.querySelector("#ot_id").value = id;
}