<?php
$qry = "SELECT id, name FROM regions;";

$stmt = $connection->prepare($qry);
$stmt->execute();
$result = $stmt->get_result();
$region_selector = '<option value="" selected>Seleccione</option>';

foreach ($result->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $region_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
}

$stmt->close();
echo $region_selector;
?>