<?php 
session_start();
$GLOBALS["environment"] = $_SERVER["HTTP_HOST"] ==  "frenosdonmario.cl"?"prod":"local";

if (!isset($_SESSION["user_id"])) {
    if ($GLOBALS["environment"] == "prod") {
        header("location: ../login/");
    } else {
        header("location: ../views/login/");
    }
}

?>