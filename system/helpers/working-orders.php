<?php
if ($esDispositivoMovil) {
    $qry_1 = 
        "SELECT wo.id AS ot, DATE(wo.created_at) AS created_at, group_concat(IFNULL(s.name, '') SEPARATOR '<br>') AS service, wost.name AS order_status, vb.name AS brand, vm.name AS model, vt.name AS type, v.vehicle_year, v.color , c.name AS customer_name, c.phone, c.email 
        FROM working_orders wo
        LEFT JOIN working_orders_services wos
            ON wo.id = wos.working_order_id
        LEFT JOIN services s 
            ON wos.service_id = s.id 
        JOIN vehicles v 
            ON wo.vehicle_id = v.id 
        JOIN vehicle_models vm 
            ON v.vehicle_model_id = vm.id
        JOIN vehicle_brands vb 
            ON vm.vehicle_brand_id = vb.id
        JOIN vehicle_types vt 
            ON v.vehicle_type_id = vt.id
        JOIN customers c 
            ON v.customer_id = c.id
        JOIN working_order_status wost
            ON wo.working_order_status_id = wost.id
        GROUP BY wo.id;";
    $stmt_1 = $connection->prepare($qry_1);
    $stmt_1->execute();
    $result_1 = $stmt_1->get_result();

    $html = '<table class="table table-hover mt-3" style="font-size: 8px;">
                <thead>
                    <th>#</th>
                    <th>OT</th>
                    <th>Servicios</th>
                    <th>Vehiculo</th>
                    <th>Opción</th>
                </thead>
                <tbody>';

    if ($result_1->num_rows == 0) {
        $html .= '<tr><td colspan="5"></td></tr>';
    } else {
        $counter = 0;
        while ($data = $result_1->fetch_object()) {
            $counter++;
            $html .= 
                '<tr>
                    <td>'.$counter.'</td>
                    <td>'.$data->ot.'<br>'.$data->created_at.'</td>
                    <td align="left">'.$data->service.'</td>
                    <td>'.$data->brand.'<br>'.$data->model.'<br>'.$data->vehicle_year.'<br>'.$data->color.'<br><b>'.$data->customer_name.'</b></td>
                    <td><button class="btn btn-sm btn-dark" title="Agregar Servicio" data-bs-toggle="modal" data-bs-target="#AddService" onclick="setWorkingIdForServices('.$data->ot.');">Agregar Servicio</button></td>
                </tr>';
        }
    }

    $html .= '</tbody></table>';
} else {
    $qry_1 = 
        "SELECT wo.id AS ot, DATE(wo.created_at) AS created_at, group_concat('- ', IFNULL(s.name, '') SEPARATOR '<br>') AS service, wost.name AS order_status, vb.name AS brand, vm.name AS model, vt.name AS type, v.vehicle_year, v.color , c.name AS customer_name, c.phone, c.email 
        FROM working_orders wo
        LEFT JOIN working_orders_services wos
            ON wo.id = wos.working_order_id
        LEFT JOIN services s 
            ON wos.service_id = s.id 
        JOIN vehicles v 
            ON wo.vehicle_id = v.id 
        JOIN vehicle_models vm 
            ON v.vehicle_model_id = vm.id
        JOIN vehicle_brands vb 
            ON vm.vehicle_brand_id = vb.id
        JOIN vehicle_types vt 
            ON v.vehicle_type_id = vt.id
        JOIN customers c 
            ON v.customer_id = c.id
        JOIN working_order_status wost
            ON wo.working_order_status_id = wost.id
        GROUP BY wo.id;";
    $stmt_1 = $connection->prepare($qry_1);
    $stmt_1->execute();
    $result_1 = $stmt_1->get_result();

    $html = '<table class="table table-hover mt-3">
                <thead>
                    <th>#</th>
                    <th>OT</th>
                    <th>Ingreso</th>
                    <th>Servicios</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Año</th>
                    <th>Color</th>
                    <th>Cliente</th>
                    <th>Opción</th>
                </thead>
                <tbody>';

    if ($result_1->num_rows == 0) {
        $html .= '<tr><td colspan="10"></td></tr>';
    } else {
        $counter = 0;
        while ($data = $result_1->fetch_object()) {
            $counter++;
            $html .= 
                '<tr>
                    <td>'.$counter.'</td>
                    <td>'.$data->ot.'</td>
                    <td>'.$data->created_at.'</td>
                    <td align="left">'.$data->service.'</td>
                    <td>'.$data->brand.'</td>
                    <td>'.$data->model.'</td>
                    <td>'.$data->vehicle_year.'</td>
                    <td>'.$data->color.'</td>
                    <td>'.$data->customer_name.'</td>
                    <td><button class="btn btn-sm btn-dark" title="Agregar Servicio" data-bs-toggle="modal" data-bs-target="#AddService" onclick="setWorkingIdForServices('.$data->ot.');">Agregar Servicio</button></td>
                </tr>';
        }
    }

    $html .= '</tbody></table>';
}

$stmt_1->close();

?>