<?php
$qry = "SELECT id, name, description FROM services ORDER BY name;";

$stmt = $connection->prepare($qry);
$stmt->execute();
$result = $stmt->get_result();
$services_selector = '<option value="" selected>Seleccione</option>';

foreach ($result->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $services_selector .= '<option value="'.$data["id"].'">'.$data["name"].' ('.$data["description"].')</option>';
}

$stmt->close();
?>