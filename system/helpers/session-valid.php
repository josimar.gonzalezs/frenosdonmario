<?php 
session_start();
// ini_set("display_errors", true);

if (!isset($_POST["username"]) && !isset($_POST["password"])) {
    if (!isset($_SESSION["user_id"])) {
        header("location: views/login/");
    } else {
        require 'databases/connections.php';
        
        $qry = 
            "SELECT u.id, u.username
            FROM users u
            JOIN user_branches ub
                ON u.id = ub.user_id
            WHERE u.id = ?
            GROUP BY u.id;";
        $stmt = $connection->prepare($qry);
        $stmt->bind_param("i", $_SESSION["user_id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        
        if ($result->num_rows == 0) {
            header("location: views/login/");
        } else {
            $result__ = $result->fetch_object();
            $_SESSION["user_id"] = $result__->id;
            $_SESSION["username"] = $result__->username;
        }
    }
} else {
    include '../databases/connections.php';
    
    $username = filter_var ($_POST["username"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $password = filter_var ($_POST["password"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $qry = 
        "SELECT u.id, u.username
        FROM users u
        JOIN user_branches ub
            ON u.id = ub.user_id
        WHERE u.username = ?
            AND u.password = SHA(?)
            AND ub.active = 1
        GROUP BY u.id;";
    $stmt = $connection->prepare($qry);
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    $result = $stmt->get_result();
    
    if ($result->num_rows > 0) {
        $result__ = $result->fetch_object();
        $_SESSION["user_id"] = $result__->id;
        $_SESSION["username"] = $result__->username;
        header("location: ../");
    }
}


?>