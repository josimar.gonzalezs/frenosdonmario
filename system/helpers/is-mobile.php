<?php
function esDispositivoMovil() {
    $agenteUsuario = $_SERVER['HTTP_USER_AGENT'];
    $dispositivosMoviles = array('iPhone', 'Android', 'webOS', 'BlackBerry', 'iPod', 'Opera Mini', 'IEMobile');

    foreach ($dispositivosMoviles as $dispositivo) {
        if (stripos($agenteUsuario, $dispositivo) !== false) {
            return true;
        }
    }
    return false;
}

$esDispositivoMovil = esDispositivoMovil();
?>