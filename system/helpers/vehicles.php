<?php
$qry_vehicle_brand = "SELECT id, name FROM vehicle_brands ORDER BY name;";

$stmt_vehicle_brand = $connection->prepare($qry_vehicle_brand);
$stmt_vehicle_brand->execute();
$result_vehicle_brand = $stmt_vehicle_brand->get_result();
$vehicle_brand_selector = '<option value="" selected>Seleccione</option>';

foreach ($result_vehicle_brand->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $vehicle_brand_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
}

$stmt_vehicle_brand->close();


$qry_vehicle_type = "SELECT id, name FROM vehicle_types ORDER BY name;";

$stmt_vehicle_type = $connection->prepare($qry_vehicle_type);
$stmt_vehicle_type->execute();
$result_vehicle_type = $stmt_vehicle_type->get_result();
$vehicle_type_selector = '<option value="" selected>Seleccione</option>';

foreach ($result_vehicle_type->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $vehicle_type_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
}

$stmt_vehicle_type->close();


?>