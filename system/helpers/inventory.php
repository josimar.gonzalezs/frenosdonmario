<?php
$qry_1 = "SELECT * FROM customers;";
$stmt_1 = $connection->prepare($qry_1);
$stmt_1->execute();
$result_1 = $stmt_1->get_result();

$html = '<table class="table table-hover mt-3">
            <thead>
                <th>#</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Teléfono</th>
                <th>Vehículos</th>
            </thead>
            <tbody>';

if ($result_1->num_rows == 0) {
    $html .= '<tr><td colspan="5"></td></tr>';
} else {
    $counter = 0;
    while ($data = $result_1->fetch_object()) {
        $counter++;
        $html .= 
            '<tr>
                <td>'.$counter.'</td>
                <td>'.$data->name.'</td>
                <td>'.$data->email.'</td>
                <td>'.$data->phone.'</td>
                <td>0</td>
            </tr>';
    }
}

$html .= '</tbody></table>';

$stmt_1->close();

?>