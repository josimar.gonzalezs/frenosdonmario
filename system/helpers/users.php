<?php
$qry = 
    "SELECT u.id, u.name
    FROM users u
    JOIN user_branches ub
        ON u.id = ub.user_id
    WHERE ub.active = 1
    GROUP BY u.id;";

$stmt = $connection->prepare($qry);
$stmt->execute();
$result = $stmt->get_result();
$user_selector = '<option value="" selected>Seleccione</option>';

foreach ($result->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $user_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
}

$stmt->close();
echo $user_selector;
?>