<?php
$qry_1 = 
    "SELECT c.*, COUNT(v.id) AS vehiclesAccount
    FROM customers c
    LEFT JOIN vehicles v
        ON c.id = v.customer_id
    GROUP BY c.id;";
$stmt_1 = $connection->prepare($qry_1);
$stmt_1->execute();
$result_1 = $stmt_1->get_result();

if ($esDispositivoMovil) {
    $html = '<table class="table table-hover mt-3" style="font-size: 8px;">
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Vehículos</th>
                    <th>Agregar Vehículos</th>
                </thead>
                <tbody>';

    if ($result_1->num_rows == 0) {
        $html .= '<tr><td colspan="4"></td></tr>';
    } else {
        $counter = 0;
        while ($data = $result_1->fetch_object()) {
            $counter++;
            $html .= 
                '<tr>
                    <td>'.$counter.'</td>
                    <td><b>'.$data->name.'</b><br>'.$data->email.'<br>'.$data->phone.'</td>
                    <td><b style="cursor: pointer;" title="ver vehículos" onmouseover="this.classList.add(\'underlined\')" onmouseout="this.classList.remove(\'underlined\')" onclick="setCustomerIdForVehicleGet('.$data->id.');">'.$data->vehiclesAccount.'</b></td>
                    <td><button class="btn btn-sm btn-dark" title="Agregar Vehículo" data-bs-toggle="modal" data-bs-target="#AddVehicle" onclick="setCustomerIdForVehicle('.$data->id.');">Agregar</button></td>
                </tr>';
        }
    }

    $html .= '</tbody></table>';
} else {
    $html = '<table class="table table-hover mt-3">
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Vehículos</th>
                    <th>Agregar Vehículos</th>
                </thead>
                <tbody>';

    if ($result_1->num_rows == 0) {
        $html .= '<tr><td colspan="6"></td></tr>';
    } else {
        $counter = 0;
        while ($data = $result_1->fetch_object()) {
            $counter++;
            $html .= 
                '<tr>
                    <td>'.$counter.'</td>
                    <td>'.$data->name.'</td>
                    <td>'.$data->email.'</td>
                    <td>'.$data->phone.'</td>
                    <td><b style="cursor: pointer;" title="ver vehículos" onmouseover="this.classList.add(\'underlined\')" onmouseout="this.classList.remove(\'underlined\')" onclick="setCustomerIdForVehicleGet('.$data->id.');">'.$data->vehiclesAccount.'</b></td>
                    <td><button class="btn btn-sm btn-dark" title="Agregar Vehículo" data-bs-toggle="modal" data-bs-target="#AddVehicle" onclick="setCustomerIdForVehicle('.$data->id.');">Agregar</button></td>
                </tr>';
        }
    }

    $html .= '</tbody></table>';
}

$stmt_1->close();

?>