<?php
$qry = "SELECT id, name FROM cities ORDER BY name;";

$stmt = $connection->prepare($qry);
$stmt->execute();
$result = $stmt->get_result();
$city_selector = '<option value="" selected>Seleccione</option>';

foreach ($result->fetch_all(MYSQLI_ASSOC) as $a => $data) {
    $city_selector .= '<option value="'.$data["id"].'">'.$data["name"].'</option>';
}

$stmt->close();
echo $city_selector;
?>