<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
  <title>Intranet Frenos Don Mario</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome CSS -->
  <link href="css/all.min.css" rel="stylesheet">
  <!-- Estilos personalizados -->
  <link href="css/styles.css?ver=<?=rand()?>" rel="stylesheet">
</head>
<body>

  <!-- Barra de navegación -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="display: none;">
    <div class="container">
      <a class="navbar-brand" href="#">Tu App</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <!-- Puedes agregar elementos de la barra de navegación aquí -->
      </div>
    </div>
  </nav>

  <!-- Menú lateral -->
  <div class="sidebar">
    <div class="sidebar-logo">
      <img src="images/logo_new_1_ajustado.png" alt="Logo de la App">
    </div>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="#" onclick="setWorkplaceTitle(1);">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="setWorkplaceTitle(2);">Órdenes de trabajo (OT)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="setWorkplaceTitle(3);">Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="setWorkplaceTitle(4);">Sucursales</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="setWorkplaceTitle(5);">Inventario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" onclick="setWorkplaceTitle(6);">Usuarios</a>
      </li>
      <li class="nav-item logout-option">
        <a class="nav-link" href="#">Cerrar Sesión</a>
      </li>
    </ul>
  </div>

  <!-- Área de trabajo -->
  <div class="content">
    <!-- Contenido de la página va aquí -->
    <h2 id="workplace_title"></h2>
    <div class="container" id="workplace_container">
    </div>
  </div>

  <!-- Bootstrap JavaScript y dependencias Popper.js y jQuery (si es necesario) -->
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/axios.min.js"></script>
  <script src="js/functions.js?ver=<?=rand()?>"></script>
</body>
</html>
