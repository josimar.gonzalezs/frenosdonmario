function setWorkplaceTitle(title_id = 1) {
    let title = "";
    let workplace_title = "";

    switch (title_id) {
        case 1:
            title = "Dashboard";
            break;
        case 2:
            title = "Órdenes de trabajo (OT)";
            break;
        case 3:
            title = "Clientes";
            break;
        case 4:
            title = "Sucursales";
            break;
        case 5:
            title = "Inventario";
            break;
        case 6:
            title = "Usuarios";
            break;

        default:
            title = "Dashboard";
            break;
    }

    workplace_title = title;
    document.querySelector("#workplace_title").innerHTML = workplace_title;
}

const getComments = async (order, sku, id) => {
    const internalNotes = document.querySelector("#internalNotes");
    internalNotes.innerHTML = "";
    var data = {
        action: "comment",
        order: order,
        sku: sku,
        id: id
    }
    try {
        const response = await axios.post("dispatch-back.php", {
            params: data
        }).then((result) => result.data);
        response.data.map(comment => {
            if (comment.internal_notes || comment.ProNota) {
                comment.internal_notes ? comment.internal_notes : "";
                comment.ProNota ? comment.ProNota : "";
                internalNotes.innerHTML = `${comment.internal_notes} ${comment.ProNota} `;
            }
        })
    } catch (error) {
        console.log(error);
    }

    var modal = document.querySelector("#modal-comments");
    modal.setAttribute("data-id", id);
    modalComments = new bootstrap.Modal(modal);
    modalComments.show();
}