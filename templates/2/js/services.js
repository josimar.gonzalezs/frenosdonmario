// Obtener los elementos correspondientes
const diferenciaFrenadoLink = document.querySelector('#diferencia-frenado-link');
const diferenciaFrenadoDef = document.querySelector('#diferencia-frenado-def');

const cambioFrenosTrenLink = document.querySelector('#cambio-frenos-tren-link');
const cambioFrenosTrenDef = document.querySelector('#cambio-frenos-tren-def');

const cambioPastillasLink = document.querySelector('#cambio-pastillas-link');
const cambioPastillasDef = document.querySelector('#cambio-pastillas-def');

const rectificadoDiscosLink = document.querySelector('#rectificado-discos-link');
const rectificadoDiscosDef = document.querySelector('#rectificado-discos-def');

const cambioBalatasLink = document.querySelector('#cambio-balatas-link');
const cambioBalatasDef = document.querySelector('#cambio-balatas-def');

const reparacionCilindrosLink = document.querySelector('#reparacion-cilindros-link');
const reparacionCilindrosDef = document.querySelector('#reparacion-cilindros-def');

const cambioCaliperLink = document.querySelector('#cambio-caliper-link');
const cambioCaliperDef = document.querySelector('#cambio-caliper-def');

let ocultarServicios = () => {
    diferenciaFrenadoDef.style.display = 'none';
    cambioFrenosTrenDef.style.display = 'none';
    cambioPastillasDef.style.display = 'none';
    rectificadoDiscosDef.style.display = 'none';
    cambioBalatasDef.style.display = 'none';
    reparacionCilindrosDef.style.display = 'none';
    cambioCaliperDef.style.display = 'none';
}

// Agregar un evento de click al enlace
diferenciaFrenadoLink.addEventListener('click', () => {
    ocultarServicios();
    diferenciaFrenadoDef.style.display = 'block';
});

// Agregar un evento de click al enlace
cambioFrenosTrenLink.addEventListener('click', () => {
    ocultarServicios();
    cambioFrenosTrenDef.style.display = 'block';
});

// Agregar un evento de click al enlace
cambioPastillasLink.addEventListener('click', () => {
    ocultarServicios();
    cambioPastillasDef.style.display = 'block';
});

// Agregar un evento de click al enlace
rectificadoDiscosLink.addEventListener('click', () => {
    ocultarServicios();
    rectificadoDiscosDef.style.display = 'block';
});

// Agregar un evento de click al enlace
cambioBalatasLink.addEventListener('click', () => {
    ocultarServicios();
    cambioBalatasDef.style.display = 'block';
});

// Agregar un evento de click al enlace
reparacionCilindrosLink.addEventListener('click', () => {
    ocultarServicios();
    reparacionCilindrosDef.style.display = 'block';
});

// Agregar un evento de click al enlace
cambioCaliperLink.addEventListener('click', () => {
    ocultarServicios();
    cambioCaliperDef.style.display = 'block';
});
