<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<title>Frenos Don Mario</title>
	<!-- CSS de Bootstrap 5 -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
	<!-- CSS personalizado para el tema oscuro -->
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<!-- Barra de navegación -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="#"><img src="images/imagen_frenosdonmario_logo_web_sinfondo.png" style="width: 100px;"></a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="">Inicio</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#quienes-somos">Quienes Somos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#servicios">Nuestros Servicios</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contacto">Contacto</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Encabezado de la página -->
	<header class="bg-dark text-white py-6">
		<img src="images/imagen_frenosdonmario_acceso.jpg" width="100%" alt="Servicios de frenos">
        <!-- <div class="container">
			<h1 class="display-1">Frenos Don Mario</h1>
			<p class="lead">Especialistas en frenos para vehículos</p>
		</div> -->
	</header>

    <!-- Sección de quienes somos -->
    <section id="quienes-somos" class="bg-dark text-white py-5">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 align="center">Quienes Somos</h2>
					<p align="justified">Somos un taller de mecánica automotriz especializado en el servicio de frenos. Con más de 20 años de experiencia en el mercado, nos hemos consolidado como uno de los mejores talleres de la ciudad.Nuestro objetivo es brindar un servicio de calidad a nuestros clientes, garantizando siempre la seguridad y confiabilidad de nuestros trabajos.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<img src="images/imagen_frenos_discos_banner.jpg" alt="Imagen de Quienes Somos" class="img-fluid" width="100%">
				</div>
			</div>
        </div>
    </section>      

	<!-- Sección de servicios -->
	<section class="py-5" id="servicios">
		<div class="container">
			<h2>Nuestros Servicios</h2>
			<p>En Frenos Don Mario ofrecemos una amplia gama de servicios para el mantenimiento y reparación de frenos de vehículos. Algunos de nuestros servicios incluyen:</p>
			<ul class="servicios-list">
				<li>
					<a id="diferencia-frenado-link" style="cursor: pointer;">Diferencia de frenado</a>
					<div id="diferencia-frenado-def" class="service-def mt-4" style="display: none;">
						<p>La diferencia de frenado se refiere a la variación en la fuerza de frenado entre las ruedas delanteras y traseras de un vehículo. Si la diferencia es muy grande, puede resultar en una pérdida de control del vehículo durante el frenado.</p>
					</div>
				</li>
				<li>
					<a id="cambio-frenos-tren-link" style="cursor: pointer;">Cambio de frenos tren delantero</a>
					<div id="cambio-frenos-tren-def" class="service-def mt-4" style="display: none;">
						<p>El cambio de frenos del tren delantero es una operación de mantenimiento que consiste en reemplazar los componentes de freno del eje delantero de un vehículo. Esto incluye discos, pastillas y calipers de freno.</p>
					</div>
				</li>
				<li>
					<a id="cambio-pastillas-link" style="cursor: pointer;">Cambio de pastillas</a>
					<div id="cambio-pastillas-def" class="service-def mt-4" style="display: none;">
						<p>El cambio de pastillas de freno es una operación de mantenimiento que consiste en reemplazar las pastillas de freno del vehículo. Las pastillas de freno son los componentes que hacen contacto con los discos de freno para detener el vehículo.</p>
					</div>
				</li>
				<li>
					<a id="rectificado-discos-link" style="cursor: pointer;">Rectificado de discos</a>
					<div id="rectificado-discos-def" class="service-def mt-4" style="display: none;">
						<p>El rectificado de discos es un proceso mediante el cual se elimina la superficie desgastada de los discos de freno para obtener una superficie plana y uniforme, lo que garantiza un frenado más efectivo y seguro. Este servicio se realiza cuando los discos presentan un desgaste excesivo o alguna deformación.</p>
					</div>
				</li>
				<li>
					<a id="cambio-balatas-link" style="cursor: pointer;">Cambio de balatas</a>
					<div id="cambio-balatas-def" class="service-def mt-4" style="display: none;">
						<p>Las balatas son las piezas que ejercen la fricción sobre los discos de freno para detener el vehículo. Este servicio consiste en reemplazar las balatas desgastadas por unas nuevas para asegurar un buen frenado. Es importante realizar este servicio periódicamente para evitar daños en otros componentes del sistema de frenos.</p>
					</div>
				</li>
				<li>
					<a id="reparacion-cilindros-link" style="cursor: pointer;">Reparación de cilindros</a>
					<div id="reparacion-cilindros-def" class="service-def mt-4" style="display: none;">
						<p>Los cilindros son los encargados de presionar las balatas contra los discos de freno para detener el vehículo. Cuando los cilindros presentan fugas o están dañados, pueden ocasionar un mal funcionamiento del sistema de frenos. Este servicio consiste en reparar o reemplazar los cilindros para garantizar un frenado seguro.</p>
					</div>
				</li>
				<li>
					<a id="cambio-caliper-link" style="cursor: pointer;">Reparación y cambio de caliper</a>
					<div id="cambio-caliper-def" class="service-def mt-4" style="display: none;">
						<p>El caliper es el componente que contiene las balatas y ejerce la presión sobre los discos de freno para detener el vehículo. Cuando el caliper está dañado, puede causar problemas de frenado, como vibraciones o ruidos. Este servicio consiste en reparar o reemplazar los calipers para garantizar un frenado seguro y efectivo.</p>
					</div>
				</li>
			</ul>
		</div>
	</section>

	<!-- Sección de contacto -->
	<section id="contacto" class="bg-dark text-white py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-6 mx-auto">
              <h2 class="text-center mb-4">Contáctanos</h2>
              <form action="mailto:josimar.gonzalez@gmail.com" method="post" enctype="text/plain">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Nombre" name="Nombre" required>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Email" name="Email" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Asunto" name="Asunto" value="Contacto <?php echo date('YmdHis'); ?>" readonly>
                </div>
                <div class="form-group">
                  <textarea class="form-control" rows="5" placeholder="Mensaje" name="Mensaje" required></textarea>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-danger">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </section>

<script src="js/services.js" type="text/javascript"></script>

</body>
<br>
<footer class="py-3">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p>Teléfono: <a href="tel:+5622953525" class="text-white">(+562) 2953 525</a></p>
        <p>Dirección: <a href="https://www.google.com/maps?q=Bah%C3%ADa+Inglesa+1512,+La+Florida,+Regi%C3%B3n+Metropolitana,+Chile" class="text-white" target="_blank">Bahía Inglesa 1512 comuna de La Florida, Santiago, Región Metropolitana, Chile.</a></p>
      </div>
      <div class="col-md-6 text-md-end">
        <p>Desarrollado por <a href="https://www.develcode.cl" class="text-white" target="_blank">Develcode.cl</a></p>
      </div>
    </div>
  </div>
</footer>
</html>
