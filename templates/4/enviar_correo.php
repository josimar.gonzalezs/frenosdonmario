<?php
    session_start();
    if(isset($_POST['grid-email'])) {
        // Recibe los datos del formulario
        $name = $_POST['grid-name'];
        $email = $_POST['grid-email'];
        $message = $_POST['grid-message'];
        
        // Configura los detalles del correo
        $destinatario = 'frenosdonmario@gmail.com';
        $asunto = 'Mensaje del formulario de contacto cliente '.$email;

        // Construye el cuerpo del correo
        $cuerpo = "Nombre cliente: $name\n";
        $cuerpo .= "Email cliente: $email\n\n";
        $cuerpo .= "Mensaje cliente: $message\n";

        // Envía el correo
        $headers = "From: Contacto <contacto@frenosdonmario.cl>";
        // $headers .= "Reply-To: $email\r\n";
        
        if(mail($destinatario, $asunto, $cuerpo, $headers)) {
            $_SESSION["validador_respuesta_correo"] = 1;
            $_SESSION["respuesta_correo"] = 'Mensaje enviado correctamente.';
        } else {
            $_SESSION["validador_respuesta_correo"] = 0;
            $_SESSION["respuesta_correo"] = 'Error al enviar el mensaje. Por favor, inténtalo de nuevo más tarde.';
        }
    }

    header("Location: ./#contacto");
?>
