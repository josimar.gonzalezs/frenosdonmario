<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/icons/ms-icon-144x144.png">
    <title>Taller de Frenos Don Mario</title>
    <!-- Tailwind CSS CDN -->
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.16/dist/tailwind.min.css" rel="stylesheet">
    <!-- Font Awesome CDN -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
    <!-- Flowbite CDN -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.8.1/flowbite.min.css" rel="stylesheet" />
    <style>
        /* Estilos personalizados para el tema oscuro */
        body {
            background-color: #000;
            color: #fff;
        }

        /* Estilo del encabezado */
        header {
            background-color: #111;
        }

        /* Estilo del menú de navegación */
        nav {
            background-color: #111;
        }

        /* Estilos de enlaces en el menú de navegación */
        nav a {
            color: #fff;
        }

        /* Estilo de la sección de servicios */
        .bg-service {
            background-color: #111;
        }

        /* Estilos de los elementos del servicio */
        .service-card {
            background-color: #222;
        }

        /* Estilo del pie de página */
        footer {
            /* background-color: #111; */
            background-color: #000;
            color: #fff;
        }

        /* Estilo del pie de página */
        .section_web {
            /* background-color: #111; */
            background-color: #000;
            color: #fff;
        }
        .main_section {
            /* Background pattern from Toptal Subtle Patterns */
            /* background-image: url("./images/imagen_frenosdonmario_acceso.jpg"); */
            background-image: url("./images/imagen_frenos_discos_banner.jpg");
            /* height: 400px; */
            width: 100%;
            background-repeat: no-repeat;
            object-fit: contain;
            /* opacity: 0.6; */
            background-size: 100%;
            box-sizing: border-box;
            /* background-blend-mode: darken; */
        }
        html {
            scroll-behavior: smooth;
        }
    </style>
</head>
<body class="bg-gray-900 text-white">

<!-- Navbar -->
<nav class="p-5">
    <div class="container mx-auto flex justify-between items-center">
        <a href="#" class="text-white text-2xl font-semibold"><img src="images/logo_new_1_ajustado.png" width="20%"></a>
        <ul class="flex space-x-5">
            <li><a href="#inicio" class="text-white">Inicio</a></li>
            <li><a href="#quienes_somos" class="text-white">Quienes&nbsp;Somos</a></li>
            <li><a href="#servicios" class="text-white">Servicios</a></li>
            <li><a href="#sucursales" class="text-white">Sucursales</a></li>
            <li><a href="#contacto" class="text-white">Contacto</a></li>
        </ul>
    </div>
</nav>

<!-- Hero Section -->
<section class="bg-gray-800 main_section text-white py-16 md:py-20" id="inicio">
    <div class="container mx-auto text-center">
        <h1 class="text-2xl md:text-4xl lg:text-5xl text-red-600 font-bold mb-4">Especialistas en Frenos</h1>
        <p class="text-base md:text-lg lg:text-xl text-red-600 font-bold mb-6">Si ya eres cliente, accede a ver los últimos trabajos realizados.</p>
        <a href="#" class="bg-red-800 text-white py-2 px-6 rounded-full font-bold hover:bg-red-600">Acceso clientes</a>
    </div>
</section>

<!-- Hero Section -->
<section class="section_web py-16 md:py-20" id="quienes_somos">
    <div class="container mx-auto text-center">
        <h2 class="text-2xl md:text-3xl font-semibold mb-4">Quienes Somos</h2>
        <p class="text md:text-base mb-6">Somos un taller de mecánica automotriz especializado en el servicio de frenos. Con más de 20 años de experiencia en el mercado, nos hemos consolidado como uno de los mejores talleres de la ciudad.Nuestro objetivo es brindar un servicio de calidad a nuestros clientes, garantizando siempre la seguridad y confiabilidad de nuestros trabajos.</p>
        <img src="images/imagen_frenosdonmario_acceso.jpg" alt="Imagen de Quienes Somos" class="img-fluid" width="100%">
    </div>
</section>

<!-- Services Section -->
<section class="section_web py-12 md:py-16" id="servicios">
    <div class="container mx-auto text-center">
        <h2 class="text-2xl md:text-3xl font-semibold mb-8">Nuestros Servicios</h2>
        <div class="grid grid-cols-1 md:grid-cols-3 gap-6 md:gap-8">
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-tools text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Diferencia de frenado</h3>
                <p class="text-sm md:text-base">Optimizamos sus frenos para un frenado uniforme y seguro en todas las ruedas, evitando el desgaste desigual y mejorando la estabilidad.</p>
            </div>
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fa-solid fa-car-rear text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Cambio de frenos tren delantero</h3>
                <p class="text-sm md:text-base">Reemplazamos los frenos delanteros de su vehículo con componentes nuevos y de calidad. Esto mejora significativamente la capacidad de frenado, la seguridad y el rendimiento de su automóvil, garantizando un frenado eficiente y confiable en las ruedas delanteras.</p>
            </div>
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-car-crash text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Cambio de pastillas</h3>
                <p class="text-sm md:text-base">Sustituimos las pastillas de freno desgastadas de su vehículo con pastillas nuevas de alta calidad. Esto mejora la capacidad de frenado y la seguridad, asegurando un frenado suave y eficaz, y evitando el desgaste excesivo de los discos de freno.</p>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-3 gap-6 md:gap-8 mt-5">
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-compact-disc text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Rectificado de discos</h3>
                <p class="text-sm md:text-base">Realizamos el rectificado de los discos de freno de su vehículo para restaurar su superficie de frenado de manera uniforme. Este proceso mejora la eficiencia de frenado, elimina vibraciones y prolonga la vida útil de los discos, garantizando un frenado seguro y suave.</p>
            </div>
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-arrow-right-to-bracket text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Cambio de balatas</h3>
                <p class="text-sm md:text-base">Sustituimos las balatas gastadas de su vehículo por balatas nuevas de alta calidad. Esto mejora la capacidad de frenado y la seguridad, asegurando un rendimiento óptimo del sistema de frenos y una conducción más segura.</p>
            </div>
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-vial text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Reparación de cilindros</h3>
                <p class="text-sm md:text-base">Realizamos la reparación de los cilindros de freno de su vehículo para restaurar su funcionalidad y rendimiento. Esto garantiza un sistema de frenado eficiente y seguro, evitando problemas como fugas de líquido de frenos y asegurando un control óptimo durante la frenada.</p>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-3 gap-6 md:gap-8 mt-5">
            <div class="p-6 bg-gray-800 rounded-lg shadow">
                <i class="fas fa-truck-ramp-box text-gray-500 text-2xl md:text-3xl mb-3 md:mb-4"></i>
                <h3 class="text-lg md:text-xl font-semibold mb-2">Reparación y cambio de caliper</h3>
                <p class="text-sm md:text-base">Ofrecemos servicio de reparación y reemplazo de calipers de freno en su vehículo. Esto asegura que los calipers, responsables de apretar las pastillas de freno contra los discos, funcionen correctamente. Garantizamos un sistema de frenado confiable y seguro para su vehículo.</p>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section class="bg-gray-800 py-12 md:py-16" id="contacto">
    <?php if(isset($_SESSION['validador_respuesta_correo'])):?>
        <?php if($_SESSION['validador_respuesta_correo'] == 1):?>
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-gray-900 px-4 py-3 shadow-md" role="alert">
                <div class="flex">
                    <div class="py-1"><svg class="fill-current h-6 w-6 text-gray-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                    <div>
                        <p class="font-bold">Enviado</p>
                        <p class="text-sm"><?=$_SESSION['respuesta_correo']?></p>
                    </div>
                </div>
            </div><br>
        <?php else: ?>
            <div role="alert">
                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                    Error
                </div>
                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                    <p><?=$_SESSION['respuesta_correo']?></p>
                </div>
            </div><br>
        <?php endif; ?>
        <?php unset($_SESSION); session_destroy(); ?>
    <?php endif; ?>
    <div class="container mx-auto text-center">
        <h2 class="text-2xl md:text-3xl font-semibold mb-4">Contáctanos</h2>
        <p class="text md:text-base mb-6">¿Listo para obtener un servicio de calidad para tus frenos? ¡Contáctanos hoy mismo!</p>
        <button class="border-2 border-white py-2 px-6 rounded-full font-semibold hover:bg-white hover:text-gray-900 transition duration-300" data-modal-target="defaultModal" data-modal-toggle="defaultModal" type="button">Enviar Mensaje</button>
    </div>
</section>

<!-- Main modal -->
<div id="defaultModal" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative w-full max-w-2xl max-h-full">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                    Formulario de contacto
                </h3>
                <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="defaultModal">
                    <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <form class="w-full max-w-lg" action="./enviar_correo.php" method="POST">
                <div class="p-6 space-y-6">
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-name">
                            Nombre
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-name" name="grid-name" type="text" placeholder="Juan Perez.">
                        </div>
                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-email">
                            Email
                            </label>
                            <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-email" name="grid-email" type="email" placeholder="juan.perez@correo.com">
                        </div>
                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-message">
                            Mensaje
                            </label>
                            <textarea class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-message" name="grid-message" type="text" placeholder="Necesito una cotización de frenos tren delantero para un Kia modelo besta año 2007"></textarea>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                    <button data-modal-hide="defaultModal" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Enviar</button>
                    <button data-modal-hide="defaultModal" type="button" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="px-8 py-8" id="sucursales">
    <div class="container grid grid-cols-2 gap-2">
        <h2 class="text-2xl md:text-3xl font-semibold mb-4">Sucursales</h2><br>
        <div>
            <p><strong>Sede Puente Alto</strong></p>
            <p>Teléfono: <a href="tel:+5622953525" class="text-white-900 hover:underline dark:text-white-600">(+562) 2953 525</a></p>
            <p>Dirección: <a href="https://www.google.com/maps?q=Bah%C3%ADa+Inglesa+1512,+Puente+Alto,+Regi%C3%B3n+Metropolitana,+Chile" class="text-white-900 hover:underline dark:text-white-600" target="_blank">Bahía Inglesa 1512 comuna de Puente Alto, Santiago, Región Metropolitana, Chile.</a></p>
        </div>
        <div>
            <p><strong>Sede La Florida</strong></p>
            <p>Próximamente apertura sucursal!</p>
        </div>
    </div>
    <div class="container grid grid-cols-2 gap-2 mt-5">
        <div>
            <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=bahia+inglesa+1512%2C+puente+alto.+Santiago.+Chile.&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><style>.mapouter{position: relative;text-align: right;width: 100%;}</style><style>.gmap_canvas{overflow: hidden;background: none !important;width: 100%;}</style></div></div>
        </div>
        <div>
            
        </div>
    </div>
    <div class="container grid grid-cols-1 gap-1">
        <div class="mt-10">
            <hr>
            <p align="center" class="mt-5">Desarrollado por <a href="https://www.develcode.cl" class="text-white-900 hover:underline dark:text-white-600" target="_blank">Develcode.cl</a></p>
            <p align="center">&copy; 2023 Taller de Frenos Don Mario. Todos los derechos reservados.</p>
        </div>
    </div>
</footer>

<!-- Flowbite js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.8.1/flowbite.min.js"></script>
</body>
</html>
