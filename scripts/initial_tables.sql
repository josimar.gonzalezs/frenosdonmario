DROP DATABASE IF EXISTS develcod_mechanical_workshop;
CREATE DATABASE develcod_mechanical_workshop;

USE develcod_mechanical_workshop;

-- DROP TABLE IF EXISTS working_orders_services;
-- DROP TABLE IF EXISTS services;
-- DROP TABLE IF EXISTS working_orders_spareparts;
-- DROP TABLE IF EXISTS spareparts;
-- DROP TABLE IF EXISTS working_orders;
-- DROP TABLE IF EXISTS working_order_status;
-- DROP TABLE IF EXISTS vehicles;
-- DROP TABLE IF EXISTS vehicle_models;
-- DROP TABLE IF EXISTS vehicle_types;
-- DROP TABLE IF EXISTS vehicle_brands;
-- DROP TABLE IF EXISTS user_branches;
-- DROP TABLE IF EXISTS users;
-- DROP TABLE IF EXISTS branches;
-- DROP TABLE IF EXISTS companies;
-- DROP TABLE IF EXISTS customers;
-- DROP TABLE IF EXISTS addreses;
-- DROP TABLE IF EXISTS cities;
-- DROP TABLE IF EXISTS regions;

CREATE TABLE regions (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100),

    PRIMARY KEY (id)
);

/* insert regions */
INSERT INTO regions (name)
VALUES 
    ('Arica y Parinacota'),
    ('Tarapacá'),
    ('Antofagasta'),
    ('Atacama'),
    ('Coquimbo'),
    ('Valparaíso'),
    ('Metropolitana de Santiago'),
    ('Libertador General Bernardo O Higgins'),
    ('Maule'),
    ('Ñuble'),
    ('Biobío'),
    ('La Araucanía'),
    ('Los Ríos'),
    ('Los Lagos'),
    ('Aysén del General Carlos Ibáñez del Campo'),
    ('Magallanes y de la Antártica Chilena');

CREATE TABLE cities (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150),
    region_id INT NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (region_id) REFERENCES regions(id)
);

/* insert cities */
-- Insertar comunas de Chile con su respectiva región
INSERT INTO cities (name, region_id) VALUES
('Arica', 1),
('Iquique', 2),
('Antofagasta', 3),
('Copiapó', 4),
('La Serena', 5),
('Valparaíso', 6),
('Viña del Mar', 6),
('Concón', 6),
('Quilpué', 6),
('Villa Alemana', 6),
('Quintero', 6),
('Puchuncaví', 6),
('Casablanca', 6),
('La Ligua', 6),
('Cabildo', 6),
('Papudo', 6),
('Zapallar', 6),
('Quillota', 6),
('La Calera', 6),
('Nogales', 6),
('San Antonio', 6),
('Cartagena', 6),
('El Quisco', 6),
('Algarrobo', 6),
('El Tabo', 6),
('Santo Domingo', 6),
('San Felipe', 6),
('Los Andes', 6),
('Calera', 6),
('Llay Llay', 6),
('Quilpué', 6),
('Limache', 6),
('Olmué', 6),
('San Antonio', 6),
('Valparaíso', 6),
('Villa Alemana', 6),
('Santiago', 7),
('Providencia', 7),
('Las Condes', 7),
('Maipú', 7),
('La Florida', 7), -- 41
('Puente Alto', 7), -- 42
('La Reina', 7),
('Quilicura', 7),
('La Pintana', 7),
('Peñalolén', 7),
('Ñuñoa', 7),
('Vitacura', 7),
('Renca', 7),
('Conchalí', 7),
('Independencia', 7),
('San Bernardo', 7),
('Lo Barnechea', 7),
('Huechuraba', 7),
('El Bosque', 7),
('La Cisterna', 7),
('San Joaquín', 7),
('La Granja', 7),
('Lo Espejo', 7),
('Recoleta', 7),
('Cerro Navia', 7),
('Lo Prado', 7),
('Pedro Aguirre Cerda', 7),
('Quinta Normal', 7),
('Pudahuel', 7),
('Estación Central', 7),
('Macul', 7),
('Cerrillos', 7),
('Peñaflor', 7),
('Lampa', 7),
('Colina', 7),
('Talagante', 7),
('Padre Hurtado', 7),
('Melipilla', 7),
('Buin', 7),
('Pirque', 7),
('San Miguel', 7),
('La Florida', 7),
('La Pintana', 7),
('Cerro Navia', 7),
('La Cisterna', 7),
('Independencia', 7),
('San Ramón', 7),
('Rancagua', 8),
('Machalí', 8),
('Graneros', 8),
('San Fernando', 8),
('San Vicente de Tagua Tagua', 8),
('Doñihue', 8),
('Coltauco', 8),
('Pichidegua', 8),
('Las Cabras', 8),
('Pichilemu', 8),
('Santa Cruz', 8),
('Litueche', 8),
('Nancagua', 8),
('Placilla', 8),
('Peralillo', 8),
('Peumo', 8),
('Palmilla', 8),
('Navidad', 8),
('Marchigüe', 8),
('Chépica', 8),
('Chimbarongo', 8),
('San Fernando', 8),
('Talca', 9),
('Chillán', 10),
('Concepción', 11),
('Temuco', 12),
('Valdivia', 13),
('Puerto Montt', 14),
('Coyhaique', 15),
('Punta Arenas', 16);

CREATE TABLE addresses (
    id INT AUTO_INCREMENT NOT NULL,
    address VARCHAR(255),
    city_id INT NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (city_id) REFERENCES cities(id)
);

/* insert addreses (by each branches) */
INSERT INTO addresses (address, city_id) VALUES ('Bahía Inglesa 1512', 42), ('San José de la estrella #1596', 41);

CREATE TABLE customers (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150),
    address_id INT DEFAULT NULL,
    phone VARCHAR(20) DEFAULT NULL,
    email VARCHAR(100) DEFAULT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES addresses(id)
);

CREATE TABLE companies (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    
    PRIMARY KEY (id)
);

/* insert companies */
INSERT INTO companies (name) VALUES ('FRENOS DON MARIO');

CREATE TABLE branches (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    address_id INT NOT NULL,
    company_id INT NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES addresses(id),
    FOREIGN KEY (company_id) REFERENCES companies(id)
);

/* insert branches */
INSERT INTO branches (name, address_id, company_id) VALUES ('PUENTE ALTO 1', 1, 1), ('FLORIDA 1', 2, 1);

CREATE TABLE users (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    email VARCHAR(150) DEFAULT NULL,
    phone VARCHAR(55) DEFAULT NULL,
    username VARCHAR(55) NOT NULL,
    password VARCHAR(255) NOT NULL,
    company_id INT NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (company_id) REFERENCES companies(id)
);

/* insert users */
INSERT INTO users (name, username, password, company_id) VALUES ('Mario Cortés', 'mario', SHA('Mario2024.'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Felipe Cortés', 'felipe', SHA('Felipe2024*'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Cristopher Cortés', 'cristopher', SHA('Cristopher2024#'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Carlos Durán', 'carlos', SHA('Carlos2024$'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Agustín', 'agustin', SHA('Agustin..2024.'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Taller Puente 1', 'puente1', SHA('Puente1*2024$'), 1);
INSERT INTO users (name, username, password, company_id) VALUES ('Taller La Florida 1', 'florida1', SHA('Florida1*2024$'), 1);

CREATE TABLE user_branches (
    user_id INT NOT NULL,
    branch_id INT NOT NULL,
    active INT NOT NULL DEFAULT 1,
    
    PRIMARY KEY (user_id, branch_id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (branch_id) REFERENCES branches(id)
);

/* insert user_branches */
INSERT INTO user_branches (user_id, branch_id, active) VALUES (1, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (1, 2, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (2, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (2, 2, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (3, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (3, 2, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (4, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (4, 2, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (5, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (5, 2, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (6, 1, 1);
INSERT INTO user_branches (user_id, branch_id, active) VALUES (7, 2, 1);

CREATE TABLE vehicle_brands (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    
    PRIMARY KEY (id)
);

/* insert vehicle_brands */
INSERT INTO vehicle_brands (name) VALUES 
('Toyota'),
('Ford'),
('Chevrolet'),
('Honda'),
('Nissan'),
('Volkswagen'),
('BMW'),
('Mercedes-Benz'),
('Audi'),
('Hyundai'),
('Kia'),
('Subaru'),
('Mazda'),
('Lexus'),
('Volvo'),
('Jeep'),
('Fiat'),
('Mitsubishi'),
('Porsche'),
('Land Rover'),
('Chrysler'),
('Jaguar'),
('Buick'),
('Cadillac'),
('Dodge'),
('Acura'),
('Infiniti'),
('Lincoln'),
('Mini'),
('Tesla'),
('Ram'),
('GMC'),
('Alfa Romeo'),
('Suzuki'),
('Ferrari'),
('Bentley'),
('Rolls-Royce'),
('Maserati'),
('McLaren'),
('Bugatti'),
('Lamborghini'),
('Aston Martin'),
('Lotus'),
('Smart'),
('Daihatsu'),
('Seat'),
('Saab'),
('Pontiac'),
('Oldsmobile'),
('Hummer'),
('Daewoo'),
('Saturn'),
('Scion'),
('MG'),
('Geo'),
('Acura'),
('Isuzu'),
('Maybach'),
('SsangYong'),
('Citroën'),
('Peugeot'),
('Renault'),
('Opel'),
('Fiat'),
('Lancia'),
('Dacia'),
('Datsun'),
('Skoda'),
('Proton'),
('Geely'),
('Great Wall'),
('Chery'),
('BYD'),
('Haval'),
('BAIC'),
('Zotye'),
('Foton'),
('JAC'),
('Changan'),
('Brilliance'),
('Karry'),
('Lifan'),
('Haima'),
('Changhe'),
('Wuling'),
('Soueast'),
('Landwind'),
('Yema'),
('ZNA'),
('JMC'),
('Leyland'),
('Tata'),
('Mahindra'),
('Ashok Leyland'),
('Maruti'),
('Force Motors');


CREATE TABLE vehicle_types (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    
    PRIMARY KEY (id)
);

/* insert vehicle_types */
INSERT INTO vehicle_types (name) 
VALUES
    ('Carros, remolques y casas rodantes'),
    ('Citycar'),
    ('Crossover'),
    ('Hatchback'),
    ('Pick up o camionetas'),
    ('Sedán'),
    ('Station Wagon'),
    ('SUV');

CREATE TABLE vehicle_models (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    vehicle_brand_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (vehicle_brand_id) REFERENCES vehicle_brands(id)
);

/* insert vehicle_models */
-- Toyota
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Corolla', 1),
('Camry', 1),
('RAV4', 1),
('Yaris', 1),
('Highlander', 1),
('Sienna', 1),
('Tacoma', 1),
('4Runner', 1),
('Prius', 1),
('Tundra', 1),
('Avalon', 1),
('Sequoia', 1),
('C-HR', 1),
('Land Cruiser', 1),
('Supra', 1),
('Mirai', 1),
('Matrix', 1),
('Celica', 1),
('Echo', 1),
('MR2', 1);

-- Ford
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('F-150', 2),
('Focus', 2),
('Escape', 2),
('Explorer', 2),
('Mustang', 2),
('Edge', 2),
('Fusion', 2),
('Ranger', 2),
('Expedition', 2),
('Taurus', 2),
('Bronco', 2),
('EcoSport', 2),
('Excursion', 2),
('Flex', 2),
('Transit', 2),
('Crown Victoria', 2),
('Thunderbird', 2),
('Windstar', 2),
('Aspire', 2),
('Probe', 2);

-- Chevrolet
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Silverado', 3),
('Equinox', 3),
('Malibu', 3),
('Camaro', 3),
('Tahoe', 3),
('Traverse', 3),
('Colorado', 3),
('Suburban', 3),
('Impala', 3),
('Blazer', 3),
('Express', 3),
('Volt', 3),
('Sonic', 3),
('Trailblazer', 3),
('Cruze', 3),
('Trax', 3),
('Cavalier', 3),
('Monte Carlo', 3),
('Venture', 3),
('Avalanche', 3);

-- Honda
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Civic', 4),
('Accord', 4),
('CR-V', 4),
('Pilot', 4),
('Odyssey', 4),
('Fit', 4),
('HR-V', 4),
('Ridgeline', 4),
('Insight', 4),
('Element', 4),
('Passport', 4),
('S2000', 4),
('CR-Z', 4),
('Clarity', 4),
('Crosstour', 4),
('City', 4),
('Legend', 4),
('Prelude', 4),
('CRX', 4),
('Integra', 4);

-- Nissan
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Altima', 5),
('Sentra', 5),
('Rogue', 5),
('Pathfinder', 5),
('Maxima', 5),
('Versa', 5),
('Murano', 5),
('Titan', 5),
('Frontier', 5),
('Armada', 5),
('370Z', 5),
('Kicks', 5),
('NV200', 5),
('Juke', 5),
('Leaf', 5),
('Quest', 5),
('Cube', 5),
('Xterra', 5),
('NV', 5),
('Rogue Sport', 5);

-- Volkswagen
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Jetta', 6),
('Passat', 6),
('Tiguan', 6),
('Atlas', 6),
('Golf', 6),
('Beetle', 6),
('Arteon', 6),
('Touareg', 6),
('CC', 6),
('Rabbit', 6),
('Golf GTI', 6),
('T-Roc', 6),
('T-Cross', 6),
('Up', 6),
('e-Golf', 6),
('Touran', 6),
('Polo', 6),
('Sharan', 6),
('Scirocco', 6),
('Caddy', 6);

-- BMW
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('3 Series', 7),
('5 Series', 7),
('X3', 7),
('X5', 7),
('7 Series', 7),
('X1', 7),
('4 Series', 7),
('X6', 7),
('X7', 7),
('2 Series', 7),
('6 Series', 7),
('Z4', 7),
('i3', 7),
('8 Series', 7),
('X2', 7),
('i8', 7),
('M2', 7),
('M4', 7),
('M5', 7),
('M8', 7);

-- Mercedes-Benz
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('C-Class', 8),
('E-Class', 8),
('GLC', 8),
('GLE', 8),
('S-Class', 8),
('GLA', 8),
('CLA', 8),
('GLB', 8),
('A-Class', 8),
('G-Class', 8),
('SLC', 8),
('GLS', 8),
('CLS', 8),
('SL', 8),
('B-Class', 8),
('Metris', 8),
('G-Class 4x4', 8),
('V-Class', 8),
('EQC', 8),
('AMG GT', 8);

-- Audi
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('A4', 9),
('Q5', 9),
('A6', 9),
('Q7', 9),
('A3', 9),
('Q3', 9),
('A5', 9),
('A8', 9),
('Q8', 9),
('TT', 9),
('RS 5', 9),
('RS 3', 9),
('RS 4', 9),
('S4', 9),
('S5', 9),
('S6', 9),
('S7', 9),
('S8', 9),
('RS 7', 9),
('SQ5', 9);

-- Hyundai
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Elantra', 10),
('Tucson', 10),
('Santa Fe', 10),
('Sonata', 10),
('Kona', 10),
('Accent', 10),
('Palisade', 10),
('Venue', 10),
('Nexo', 10),
('Genesis G80', 10),
('Genesis G70', 10),
('Ioniq', 10),
('Veloster', 10),
('Genesis G90', 10),
('Azera', 10),
('Genesis Coupe', 10),
('Equus', 10),
('XG350', 10),
('Tiburon', 10),
('Veracruz', 10);

-- Kia
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Sorento', 11),
('Sportage', 11),
('Telluride', 11),
('Forte', 11),
('Soul', 11),
('Optima', 11),
('Rio', 11),
('Stinger', 11),
('Niro', 11),
('Cadenza', 11),
('K900', 11),
('Mohave', 11),
('Carnival', 11),
('Quoris', 11),
('Opirus', 11),
('Borrego', 11),
('Pride', 11),
('Joice', 11),
('Capital', 11),
('Besta', 11);

-- Subaru
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Outback', 12),
('Forester', 12),
('Legacy', 12),
('Impreza', 12),
('Crosstrek', 12),
('Ascent', 12),
('WRX', 12),
('BRZ', 12),
('XV', 12),
('Baja', 12),
('Tribeca', 12),
('Justy', 12),
('Alcyone SVX', 12),
('Leone', 12),
('Rex', 12),
('Libero', 12),
('Lucra', 12),
('Traviq', 12),
('Exiga', 12),
('Stella', 12);

-- Mazda
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Mazda3', 13),
('Mazda6', 13),
('CX-5', 13),
('CX-9', 13),
('MX-5 Miata', 13),
('CX-30', 13),
('MX-30', 13),
('CX-3', 13),
('RX-8', 13),
('MX-6', 13),
('MPV', 13),
('Millenia', 13),
('RX-7', 13),
('Tribute', 13),
('Navajo', 13),
('B-Series', 13),
('626', 13),
('808', 13),
('929', 13),
('RX-2', 13);

-- Lexus
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('RX', 14),
('ES', 14),
('NX', 14),
('IS', 14),
('GX', 14),
('UX', 14),
('LS', 14),
('RC', 14),
('LX', 14),
('GS', 14),
('LC', 14),
('CT', 14),
('HS', 14),
('SC', 14),
('LF', 14),
('LF-A', 14),
('LM', 14),
('LFA', 14),
('LF-C', 14),
('LF-X', 14);

-- Volvo
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('XC90', 15),
('XC60', 15),
('XC40', 15),
('S60', 15),
('S90', 15),
('V60', 15),
('V90', 15),
('V40', 15),
('C70', 15),
('S80', 15),
('S40', 15),
('960', 15),
('740', 15),
('850', 15),
('780', 15),
('244', 15),
('245', 15),
('265', 15),
('262', 15),
('145', 15);

-- Jeep
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Wrangler', 16),
('Grand Cherokee', 16),
('Cherokee', 16),
('Compass', 16),
('Renegade', 16),
('Gladiator', 16),
('Commander', 16),
('Patriot', 16),
('Liberty', 16),
('Wagoneer', 16),
('CJ', 16),
('Comanche', 16),
('Willys', 16),
('FC', 16),
('Scrambler', 16),
('DJ', 16),
('FJ', 16),
('J8', 16),
('Mighty FC', 16),
('J6', 16);

-- Fiat
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('500', 17),
('Panda', 17),
('Tipo', 17),
('Punto', 17),
('500X', 17),
('500L', 17),
('Ducato', 17),
('124 Spider', 17),
('Qubo', 17),
('Talento', 17),
('Doblò', 17),
('Fiorino', 17),
('Linea', 17),
('Croma', 17),
('Bravo', 17),
('Argenta', 17),
('Seicento', 17),
('Uno', 17),
('Ritmo', 17),
('126', 17);

-- Mitsubishi
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Outlander', 18),
('Pajero', 18),
('L200', 18),
('ASX', 18),
('Eclipse Cross', 18),
('Montero', 18),
('Mirage', 18),
('Colt', 18),
('Lancer', 18),
('Space Star', 18),
('Galant', 18),
('Carisma', 18),
('Grandis', 18),
('Celeste', 18),
('3000GT', 18),
('FTO', 18),
('GTO', 18),
('Pajero Sport', 18),
('Endeavor', 18),
('Raider', 18);

-- Porsche
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('911', 19),
('Cayenne', 19),
('Panamera', 19),
('Macan', 19),
('Boxster', 19),
('Cayman', 19),
('Taycan', 19),
('718', 19),
('944', 19),
('928', 19),
('918 Spyder', 19),
('959', 19),
('Carrera GT', 19),
('356', 19),
('918', 19),
('968', 19),
('914', 19),
('550 Spyder', 19),
('968 Turbo S', 19),
('RS Spyder', 19);

-- Land Rover
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Range Rover', 20),
('Discovery', 20),
('Defender', 20),
('Evoque', 20),
('Freelander', 20),
('Discovery Sport', 20),
('Range Rover Sport', 20),
('Series III', 20),
('Series II', 20),
('Series I', 20),
('Range Rover Velar', 20),
('Range Rover Evoque Convertible', 20),
('Range Rover Classic', 20),
('Range Rover Sport SVR', 20),
('Range Rover Classic Convertible', 20),
('Range Rover Evoque Coupe', 20),
('Range Rover Evoque Five-door', 20),
('Range Rover Evoque Five-door', 20),
('Range Rover Evoque Five-door', 20),
('Range Rover Evoque Five-door', 20);

-- Chrysler
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('300', 21),
('Pacifica', 21),
('Voyager', 21),
('Town & Country', 21),
('PT Cruiser', 21),
('Sebring', 21),
('Aspen', 21),
('Concorde', 21),
('Crossfire', 21),
('LHS', 21),
('New Yorker', 21),
('Cirrus', 21),
('LeBaron', 21),
('Imperial', 21),
('Neon', 21),
('Royal', 21),
('Saratoga', 21),
('Vision', 21),
('Windsor', 21),
('Yorker', 21);

-- Jaguar
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('F-Pace', 22),
('XE', 22),
('XF', 22),
('XJ', 22),
('F-Type', 22),
('E-Pace', 22),
('I-Pace', 22),
('S-Type', 22),
('X-Type', 22),
('C-Type', 22),
('D-Type', 22),
('SS', 22),
('MK I', 22),
('MK II', 22),
('XK120', 22),
('XK140', 22),
('XK150', 22),
('XK', 22),
('XJ220', 22),
('XJ-S', 22);

-- Buick
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Enclave', 23),
('Encore', 23),
('Encore GX', 23),
('Regal', 23),
('Envision', 23),
('Lacrosse', 23),
('Verano', 23),
('Cascada', 23),
('Century', 23),
('Rendezvous', 23),
('Park Avenue', 23),
('Skylark', 23),
('LeSabre', 23),
('Electra', 23),
('Roadmaster', 23),
('Reatta', 23),
('Special', 23),
('Wildcat', 23),
('Gran Sport', 23),
('Super', 23);

-- Cadillac
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Escalade', 24),
('XT5', 24),
('XT4', 24),
('XT6', 24),
('CT6', 24),
('ATS', 24),
('CTS', 24),
('CT4', 24),
('XTS', 24),
('STS', 24),
('SRX', 24),
('Seville', 24),
('DeVille', 24),
('Fleetwood', 24),
('Eldorado', 24),
('Brougham', 24),
('Cimarron', 24),
('Allanté', 24),
('Calais', 24),
('60 Special', 24);

-- Dodge
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Challenger', 25),
('Charger', 25),
('Durango', 25),
('Journey', 25),
('Grand Caravan', 25),
('Viper', 25),
('Dart', 25),
('Avenger', 25),
('Nitro', 25),
('Caliber', 25),
('Intrepid', 25),
('Magnum', 25),
('Neon', 25),
('Stealth', 25),
('Daytona', 25),
('Ramcharger', 25),
('Spirit', 25),
('Shadow', 25),
('Omni', 25),
('Diplomat', 25);

-- Acura
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('MDX', 26),
('RDX', 26),
('TLX', 26),
('ILX', 26),
('NSX', 26),
('RLX', 26),
('RSX', 26),
('CL', 26),
('Integra', 26),
('Legend', 26),
('Vigor', 26),
('TSX', 26),
('SLX', 26),
('EL', 26),
('CSX', 26),
('ZDX', 26),
('RL', 26),
('TL', 26),
('Vigour', 26),
('Precision', 26);

-- Infiniti
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Q50', 27),
('Q60', 27),
('QX60', 27),
('QX50', 27),
('QX80', 27),
('Q70', 27),
('QX30', 27),
('QX70', 27),
('FX35', 27),
('G35', 27),
('EX35', 27),
('JX35', 27),
('M35', 27),
('I30', 27),
('G20', 27),
('I35', 27),
('J30', 27),
('M45', 27),
('G25', 27),
('QX4', 27);

-- Lincoln
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Navigator', 28),
('Aviator', 28),
('Corsair', 28),
('Nautilus', 28),
('Continental', 28),
('MKX', 28),
('MKZ', 28),
('MKT', 28),
('Town Car', 28),
('LS', 28),
('Zephyr', 28),
('Mark LT', 28),
('MKC', 28),
('Blackwood', 28),
('Capri', 28),
('Versailles', 28),
('MKR', 28),
('Premiere', 28),
('K-Series', 28),
('MKX', 28);

-- Mini
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Cooper', 29),
('Countryman', 29),
('Clubman', 29),
('Convertible', 29),
('Paceman', 29),
('Coupe', 29),
('Roadster', 29),
('Rocketman', 29),
('Cabrio', 29),
('Superleggera', 29),
('Clubvan', 29),
('One', 29),
('Traveller', 29),
('Austin Mini', 29),
('Classic Mini', 29),
('Mini Cooper S', 29),
('John Cooper Works', 29),
('Cooper D', 29),
('John Cooper Works GP', 29),
('Mini Electric', 29);

-- Tesla
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Model S', 30),
('Model 3', 30),
('Model X', 30),
('Model Y', 30),
('Roadster', 30),
('Cybertruck', 30),
('Semi', 30),
('Model 2', 30),
('Model 4', 30),
('Model 5', 30),
('Model 6', 30),
('Model 7', 30),
('Model 8', 30),
('Model 9', 30),
('Model 10', 30),
('Model 11', 30),
('Model 12', 30),
('Model 13', 30),
('Model 14', 30),
('Model 15', 30);

-- Ram
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('1500', 31),
('2500', 31),
('3500', 31),
('ProMaster', 31),
('ProMaster City', 31),
('Dakota', 31),
('Power Wagon', 31),
('Ramcharger', 31),
('Ram Van', 31),
('Ram SRT-10', 31),
('Ram 50', 31),
('Ram 700', 31),
('Ram 1200', 31),
('Ram CV Tradesman', 31),
('Ram C/V', 31),
('Ram D Series', 31),
('Ram Daytona', 31),
('Ram EV', 31),
('Ram F Series', 31),
('Ram G Series', 31);

-- GMC
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Sierra', 32),
('Yukon', 32),
('Terrain', 32),
('Acadia', 32),
('Canyon', 32),
('Envoy', 32),
('Jimmy', 32),
('Savana', 32),
('Vandura', 32),
('Rally Wagon', 32),
('Typhoon', 32),
('Tracker', 32),
('Suburban', 32),
('TopKick', 32),
('Brigadier', 32),
('General', 32),
('Syclone', 32),
('Safari', 32),
('Sonoma', 32),
('Sprint', 32);

-- Alfa Romeo
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Giulia', 33),
('Stelvio', 33),
('4C', 33),
('Giulietta', 33),
('MiTo', 33),
('Spider', 33),
('159', 33),
('166', 33),
('GT', 33),
('GTV', 33),
('Brera', 33),
('Crosswagon', 33),
('SZ', 33),
('RZ', 33),
('33', 33),
('75', 33),
('Alfasud', 33),
('Arna', 33),
('33 Stradale', 33),
('6C', 33);

-- Suzuki
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Swift', 34),
('Vitara', 34),
('Jimny', 34),
('S-Cross', 34),
('Ignis', 34),
('Baleno', 34),
('SX4', 34),
('Splash', 34),
('Celerio', 34),
('Alto', 34),
('Kizashi', 34),
('X-90', 34),
('Samurai', 34),
('Grand Vitara', 34),
('Fronte', 34),
('Kei', 34),
('Liana', 34),
('Forenza', 34),
('Equator', 34),
('Aerio', 34);

-- Ferrari
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('488 GTB', 35),
('812 Superfast', 35),
('SF90 Stradale', 35),
('F8 Tributo', 35),
('Portofino', 35),
('Roma', 35),
('488 Pista', 35),
('GTC4Lusso', 35),
('458 Italia', 35),
('California T', 35),
('488 Spider', 35),
('F12 Berlinetta', 35),
('360 Modena', 35),
('F430', 35),
('Testarossa', 35),
('458 Spider', 35),
('FF', 35),
('550 Maranello', 35),
('575M Maranello', 35),
('599 GTB Fiorano', 35);

-- Bentley
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Bentayga', 36),
('Continental GT', 36),
('Flying Spur', 36),
('Mulsanne', 36),
('Arnage', 36),
('Azure', 36),
('Brooklands', 36),
('Turbo R', 36),
('Continental Flying Spur', 36),
('Continental GTC', 36),
('Eight', 36),
('Mulsanne S', 36),
('Turbo RT', 36),
('Continental R', 36),
('Continental SC', 36),
('Continental T', 36),
('Continental Azure', 36),
('Continental Supersports', 36),
('Bentley State Limousine', 36),
('EXP Speed 8', 36);

-- Rolls-Royce
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Phantom', 37),
('Cullinan', 37),
('Ghost', 37),
('Wraith', 37),
('Dawn', 37),
('Silver Shadow', 37),
('Silver Spirit', 37),
('Silver Seraph', 37),
('Silver Cloud', 37),
('Corniche', 37),
('Park Ward', 37),
('Silver Wraith', 37),
('Silver Dawn', 37),
('Camargue', 37),
('Phantom V', 37),
('Phantom VI', 37),
('Phantom VII', 37),
('Phantom VIII', 37),
('Silver Spur', 37),
('Silver Wraith II', 37);

-- Maserati
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Ghibli', 38),
('Quattroporte', 38),
('Levante', 38),
('GranTurismo', 38),
('GranCabrio', 38),
('MC20', 38),
('3200 GT', 38),
('Coupe', 38),
('Spyder', 38),
('Gransport', 38),
('Bora', 38),
('Merak', 38),
('Khamsin', 38),
('Indy', 38),
('Kyalami', 38),
('Shamal', 38),
('Biturbo', 38),
('430', 38),
('228', 38),
('3500 GT', 38);

-- McLaren
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('720S', 39),
('570S', 39),
('P1', 39),
('675LT', 39),
('650S', 39),
('600LT', 39),
('MP4-12C', 39),
('570GT', 39),
('540C', 39),
('12C', 39),
('675LT Spider', 39),
('650S Spider', 39),
('625C', 39),
('675LT Coupe', 39),
('MSO HS', 39),
('F1', 39),
('GT', 39),
('Senna', 39),
('Speedtail', 39),
('Artura', 39);

-- Bugatti
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Chiron', 40),
('Veyron', 40),
('Divo', 40),
('EB110', 40),
('Type 57', 40),
('Type 55', 40),
('Type 41', 40),
('Type 35', 40),
('Type 51', 40),
('Type 59', 40),
('Type 101', 40),
('Type 252', 40),
('Type 251', 40),
('Type 32', 40),
('Type 101C', 40),
('Type 41 Royale', 40),
('Type 50', 40),
('Type 251', 40),
('Type 50T', 40),
('Type 101 Ghia Roadster', 40);

-- Lamborghini
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Aventador', 41),
('Huracan', 41),
('Urus', 41),
('Gallardo', 41),
('Murcielago', 41),
('Diablo', 41),
('Countach', 41),
('Miura', 41),
('Jalpa', 41),
('Espada', 41),
('Urraco', 41),
('Silhouette', 41),
('Islero', 41),
('400 GT', 41),
('Jarama', 41),
('350 GT', 41),
('LM002', 41),
('Reventon', 41),
('Sesto Elemento', 41),
('Centenario', 41);

-- Aston Martin
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('DB11', 42),
('Vantage', 42),
('DBS Superleggera', 42),
('DBS', 42),
('Rapide', 42),
('Vanquish', 42),
('DB9', 42),
('Virage', 42),
('DB4', 42),
('DB5', 42),
('DB6', 42),
('Valkyrie', 42),
('Lagonda', 42),
('One-77', 42),
('DB7', 42),
('Virage', 42),
('Valhalla', 42),
('DBX', 42),
('Vulcan', 42),
('DB AR1', 42);

-- Lotus
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Elise', 43),
('Exige', 43),
('Evora', 43),
('Evija', 43),
('Esprit', 43),
('Europa', 43),
('Seven', 43),
('Elite', 43),
('Elan', 43),
('Eleven', 43),
('Eclat', 43),
('Excel', 43),
('340R', 43),
('3-Eleven', 43),
('2-Eleven', 43),
('340', 43),
('Mark I', 43),
('Mark II', 43),
('Mark III', 43),
('Mark IV', 43);

-- Smart
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Fortwo', 44),
('Forfour', 44),
('Roadster', 44),
('Crossblade', 44),
('Forstars', 44),
('Forfour', 44),
('Formore', 44),
('EB1', 44),
('EB2', 44),
('Crosstown', 44),
('Concept #1', 44),
('Forjeremy', 44),
('Forspeed', 44),
('Formore', 44),
('Crossblade', 44),
('Forstars', 44),
('EB1', 44),
('EB2', 44),
('Crosstown', 44),
('Concept #1', 44);

-- Daihatsu
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Cuore', 45),
('Sirion', 45),
('Terios', 45),
('Materia', 45),
('Copen', 45),
('Move', 45),
('Charade', 45),
('Applause', 45),
('Feroza', 45),
('Rocky', 45),
('Trevis', 45),
('Opti', 45),
('Boon', 45),
('Ayla', 45),
('Tanto', 45),
('YRV', 45),
('Naked', 45),
('Taft', 45),
('Max', 45),
('Leeza', 45);

-- Seat
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Leon', 46),
('Ibiza', 46),
('Ateca', 46),
('Arona', 46),
('Tarraco', 46),
('Alhambra', 46),
('Toledo', 46),
('Mii', 46),
('Exeo', 46),
('Cordoba', 46),
('Inca', 46),
('Ronda', 46),
('Malaga', 46),
('Marbella', 46),
('Fura', 46),
('600', 46),
('124', 46),
('800', 46),
('1200', 46),
('1400', 46);

-- Saab
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('9-3', 47),
('9-5', 47),
('900', 47),
('9000', 47),
('96', 47),
('95', 47),
('99', 47),
('900', 47),
('92', 47),
('9-2X', 47),
('9-4X', 47),
('9-7X', 47),
('900 Turbo', 47),
('99 Turbo', 47),
('9000 Aero', 47),
('9-3 Viggen', 47),
('9-5 Aero', 47),
('9-X', 47),
('9-3X', 47),
('9-X BioHybrid', 47);

-- Pontiac
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('GTO', 48),
('Firebird', 48),
('Grand Prix', 48),
('Sunfire', 48),
('Bonneville', 48),
('Trans Am', 48),
('G8', 48),
('Solstice', 48),
('Fiero', 48),
('Montana', 48),
('Aztek', 48),
('Vibe', 48),
('Torrent', 48),
('G6', 48),
('Catalina', 48),
('Tempest', 48),
('LeMans', 48),
('Grand Am', 48),
('Star Chief', 48),
('Chieftain', 48);

-- Oldsmobile
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Alero', 49),
('Intrigue', 49),
('Bravada', 49),
('Aurora', 49),
('Silhouette', 49),
('Cutlass', 49),
('Achieva', 49),
('88', 49),
('98', 49),
('Toronado', 49),
('Omega', 49),
('F-85', 49),
('Firenza', 49),
('Omega', 49),
('Cutlass Ciera', 49),
('Cutlass Supreme', 49),
('Custom Cruiser', 49),
('Starfire', 49),
('Cutlass Calais', 49),
('Cutlass Salon', 49);

-- Hummer
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('H1', 50),
('H2', 50),
('H3', 50),
('H4', 50),
('H5', 50),
('H6', 50),
('H7', 50),
('H8', 50),
('H9', 50),
('H10', 50),
('H11', 50),
('H12', 50),
('H13', 50),
('H14', 50),
('H15', 50),
('H16', 50),
('H17', 50),
('H18', 50),
('H19', 50),
('H20', 50);

-- Daewoo
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Lanos', 51),
('Matiz', 51),
('Nubira', 51),
('Espero', 51),
('Leganza', 51),
('Prince', 51),
('Racer', 51),
('Lemans', 51),
('Tico', 51),
('Magnus', 51),
('G2X', 51),
('Nexia', 51),
('Arcadia', 51),
('Fantasy', 51),
('Maepsy', 51),
('Maestro', 51),
('Mega', 51),
('Pointer', 51),
('Super Salon', 51),
('Super Fantasy', 51);

-- Saturn
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('S-Series', 52),
('Ion', 52),
('Vue', 52),
('Aura', 52),
('Sky', 52),
('L-Series', 52),
('Outlook', 52),
('Relay', 52),
('Astra', 52),
('SC', 52),
('SW', 52),
('LS', 52),
('LW', 52),
('SL', 52),
('SC1', 52),
('SC2', 52),
('SL1', 52),
('SL2', 52),
('SW1', 52),
('SW2', 52);

-- Scion
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('xB', 53),
('xA', 53),
('tC', 53),
('FR-S', 53),
('iQ', 53),
('xD', 53),
('iA', 53),
('iM', 53),
('xB RS 3.0', 53),
('xD RS 2.0', 53),
('xB RS 8.0', 53),
('FR-S RS 1.0', 53),
('tC RS 5.0', 53),
('xA RS 2.0', 53),
('xB Release Series 5.0', 53),
('xD Release Series 2.0', 53),
('FR-S Release Series 1.0', 53),
('tC Release Series 7.0', 53),
('xA Release Series 2.0', 53),
('xB Release Series 9.0', 53);

-- MG
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('MG3', 54),
('MG ZS', 54),
('MG6', 54),
('MG GS', 54),
('MG TF', 54),
('MG ZR', 54),
('MG ZT', 54),
('MG Magnette', 54),
('MG RV8', 54),
('MG XPower SV', 54),
('MG XPower SV-R', 54),
('MG 1100', 54),
('MG 1300', 54),
('MG Metro', 54),
('MG Maestro', 54),
('MG Montego', 54),
('MG Magna', 54),
('MG Midget', 54),
('MG Magnette', 54),
('MG F', 54);

-- Geo
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Metro', 55),
('Prizm', 55),
('Storm', 55),
('Tracker', 55),
('Metro LSi', 55),
('Prizm LSi', 55),
('Prizm GSi', 55),
('Tracker LSi', 55),
('Metro XFi', 55),
('Metro LSi Convertible', 55),
('Prizm LSi Sedan', 55),
('Prizm GSi Sedan', 55),
('Tracker LSi 2-Door', 55),
('Tracker 4-Door', 55),
('Tracker LSi Convertible', 55),
('Tracker LSi 4-Door', 55),
('Tracker LT 4-Door', 55),
('Tracker LT 2-Door', 55),
('Metro XFi 2-Door', 55),
('Metro LSi Hatchback', 55);

-- Acura
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('MDX', 26),
('RDX', 26),
('TLX', 26),
('ILX', 26),
('NSX', 26),
('RLX', 26),
('RSX', 26),
('CL', 26),
('Integra', 26),
('Legend', 26),
('Vigor', 26),
('TSX', 26),
('SLX', 26),
('EL', 26),
('CSX', 26),
('ZDX', 26),
('RL', 26),
('TL', 26),
('Vigour', 26),
('Precision', 26);

-- Isuzu
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Rodeo', 56),
('Trooper', 56),
('Ascender', 56),
('VehiCROSS', 56),
('Amigo', 56),
('i-Series', 56),
('Hombre', 56),
('Oasis', 56),
('Piazza', 56),
('Stylus', 56),
('Gemini', 56),
('Bighorn', 56),
('Fargo', 56),
('Faster', 56),
('MU-7', 56),
('D-Max', 56),
('MU-X', 56),
('Fargo WFR', 56),
('Fargo Filly', 56),
('Fargo Wagon', 56);

-- Maybach
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('57', 57),
('62', 57),
('57S', 57),
('62S', 57),
('Landaulet', 57),
('Exelero', 57),
('DS7', 57),
('SW35', 57),
('SW38', 57),
('SW42', 57),
('SW50', 57),
('SW52', 57),
('W3', 57),
('Zeppelin', 57),
('62 Landaulet', 57),
('W5', 57),
('W6', 57),
('W7', 57),
('SW41', 57),
('W1', 57);

-- SsangYong
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Tivoli', 58),
('Korando', 58),
('Rexton', 58),
('Musso', 58),
('Actyon', 58),
('Kyron', 58),
('Rodius', 58),
('Stavic', 58),
('Chairman', 58),
('Istana', 58),
('Rexton Sports', 58),
('Rexton W', 58),
('Korando C', 58),
('Korando Turismo', 58),
('Korando K4', 58),
('Korando K5', 58),
('Tivoli Air', 58),
('Tivoli Grand', 58),
('Tivoli K1', 58),
('Korando K6', 58);

-- Citroën
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('C3', 59),
('C4', 59),
('C5', 59),
('C1', 59),
('C2', 59),
('C6', 59),
('C8', 59),
('Berlingo', 59),
('Xsara', 59),
('Xsara Picasso', 59),
('DS3', 59),
('DS4', 59),
('DS5', 59),
('ZX', 59),
('XM', 59),
('Ax', 59),
('BX', 59),
('C15', 59),
('C25', 59),
('C35', 59);

-- Peugeot
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('208', 60),
('308', 60),
('508', 60),
('2008', 60),
('3008', 60),
('5008', 60),
('108', 60),
('207', 60),
('206', 60),
('106', 60),
('405', 60),
('406', 60),
('307', 60),
('309', 60),
('605', 60),
('607', 60),
('807', 60),
('4007', 60),
('4008', 60),
('508 RXH', 60);

-- Renault
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Clio', 61),
('Megane', 61),
('Captur', 61),
('Kadjar', 61),
('Scenic', 61),
('Twingo', 61),
('Zoe', 61),
('Talisman', 61),
('Koleos', 61),
('Espace', 61),
('Laguna', 61),
('Fluence', 61),
('Duster', 61),
('Symbol', 61),
('Modus', 61),
('Latitude', 61),
('Wind', 61),
('Vel Satis', 61),
('Thalia', 61),
('Avantime', 61);

-- Opel
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Corsa', 62),
('Astra', 62),
('Insignia', 62),
('Mokka', 62),
('Crossland', 62),
('Grandland', 62),
('Karl', 62),
('Adam', 62),
('Agila', 62),
('Zafira', 62),
('Meriva', 62),
('Vectra', 62),
('Combo', 62),
('Tigra', 62),
('Calibra', 62),
('Ampera', 62),
('Frontera', 62),
('Antara', 62),
('Movano', 62),
('Vivaro', 62);

-- Fiat
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('500', 63),
('Panda', 63),
('Tipo', 63),
('Punto', 63),
('500X', 63),
('500L', 63),
('Doblò', 63),
('Tipo', 63),
('Qubo', 63),
('124 Spider', 63),
('Bravo', 63),
('Linea', 63),
('Croma', 63),
('Seicento', 63),
('Stilo', 63),
('Multipla', 63),
('Cinquecento', 63),
('Ulysse', 63),
('Scudo', 63),
('Ducato', 63);

-- Lancia
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Ypsilon', 64),
('Delta', 64),
('Voyager', 64),
('Thema', 64),
('Phedra', 64),
('Musra', 64),
('Flavia', 64),
('Lybra', 64),
('Musa', 64),
('Kappa', 64),
('Zeta', 64),
('Dedra', 64),
('Beta', 64),
('Gamma', 64),
('Stratos', 64),
('Monte Carlo', 64),
('Fulvia', 64),
('Y10', 64),
('Appia', 64),
('Aurelia', 64);

-- Dacia
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Sandero', 65),
('Duster', 65),
('Logan', 65),
('Lodgy', 65),
('Dokker', 65),
('Duster', 65),
('Duster Oroch', 65),
('1300', 65),
('1310', 65),
('1307', 65),
('1309', 65),
('1305', 65),
('1320', 65),
('Solenza', 65),
('Nova', 65),
('Supernova', 65),
('SupeRNova', 65),
('1310 Sport', 65),
('1310 GT', 65),
('1310 Break', 65);

-- Datsun
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Go', 66),
('Go+', 66),
('Redi-Go', 66),
('on-Do', 66),
('mi-Do', 66),
('Cross', 66),
('Fairlady', 66),
('Bluebird', 66),
('Cherry', 66),
('Sunny', 66),
('Pulsar', 66),
('Skyline', 66),
('Silvia', 66),
('Maxima', 66),
('Altima', 66),
('Cedric', 66),
('Laurel', 66),
('Cedric', 66),
('Laurel', 66),
('Cedric', 66);

-- Skoda
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Octavia', 67),
('Fabia', 67),
('Superb', 67),
('Kodiaq', 67),
('Karoq', 67),
('Scala', 67),
('Kamiq', 67),
('Rapid', 67),
('Citigo', 67),
('Yeti', 67),
('Roomster', 67),
('Favorit', 67),
('Felicia', 67),
('Forman', 67),
('120', 67),
('130', 67),
('135', 67),
('105', 67),
('110', 67),
('100', 67);

-- Proton
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Saga', 68),
('Persona', 68),
('Iriz', 68),
('X70', 68),
('Preve', 68),
('Exora', 68),
('Satria', 68),
('Waja', 68),
('Gen-2', 68),
('Wira', 68),
('Juara', 68),
('Perdana', 68),
('Putra', 68),
('Arena', 68),
('Inspira', 68),
('Ertiga', 68),
('Tiara', 68),
('Compact', 68),
('Persona', 68),
('Impian', 68);

-- Geely
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Emgrand', 69),
('Boyue', 69),
('Coolray', 69),
('Borui', 69),
('Vision', 69),
('Jihe', 69),
('Jihe A', 69),
('Jihe B', 69),
('King Kong', 69),
('Otaka', 69),
('Beauty Leopard', 69),
('TX4', 69),
('Xiongmao', 69),
('MGrand X7', 69),
('MGrand X7 Sport', 69),
('MGrand X7L', 69),
('GS', 69),
('Vision X1', 69),
('Vision X3', 69),
('Vision X6', 69);

-- Great Wall
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Hover', 70),
('Haval', 70),
('Voleex', 70),
('Cowry', 70),
('Wingle', 70),
('Coolbear', 70),
('Deer', 70),
('Hover H6', 70),
('Hover H3', 70),
('Hover H5', 70),
('Hover H2', 70),
('Hover M2', 70),
('Hover M4', 70),
('Hover M1', 70),
('Hover M3', 70),
('Hover M6', 70),
('Hover M7', 70),
('Coolbear', 70),
('Voleex C10', 70),
('Voleex C20R', 70);

-- Chery
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Tiggo', 71),
('Arrizo', 71),
('QQ', 71),
('Fulwin', 71),
('A13', 71),
('Eastar', 71),
('E3', 71),
('E5', 71),
('Karry', 71),
('Skin', 71),
('Easter Cross', 71),
('J11', 71),
('J3', 71),
('T11', 71),
('J2', 71),
('J5', 71),
('J7', 71),
('Tengo', 71),
('Fora', 71),
('M11', 71);

-- BYD
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Tang', 72),
('Han', 72),
('Song', 72),
('Yuan', 72),
('Qin', 72),
('e2', 72),
('e3', 72),
('e5', 72),
('e1', 72),
('F0', 72),
('F3', 72),
('F6', 72),
('G3', 72),
('G5', 72),
('G6', 72),
('S6', 72),
('S7', 72),
('S8', 72),
('T3', 72),
('T5', 72);

-- Haval
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('H6', 73),
('H9', 73),
('H2', 73),
('H1', 73),
('H4', 73),
('H7', 73),
('H3', 73),
('H5', 73),
('F7', 73),
('F5', 73),
('F9', 73),
('F7x', 73),
('F9x', 73),
('Jolion', 73),
('Big Dog', 73),
('COUPE', 73),
('Dark Dog', 73),
('F9 Black Label', 73),
('F9 Crawler Edition', 73),
('F7 Crawler Edition', 73);

-- BAIC
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('BJ40', 74),
('BJ80', 74),
('BJ20', 74),
('BJ90', 74),
('BJ30', 74),
('EX3', 74),
('EX5', 74),
('EX6', 74),
('EU7', 74),
('EU5', 74),
('X25', 74),
('X35', 74),
('X55', 74),
('X65', 74),
('X55 PLUS', 74),
('M60', 74),
('M70', 74),
('M80', 74),
('M50', 74),
('M30', 74);

-- Zotye
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('T600', 75),
('T700', 75),
('T300', 75),
('T500', 75),
('SR7', 75),
('SR9', 75),
('SR6', 75),
('E200', 75),
('E30', 75),
('E300', 75),
('E01', 75),
('E200 Plus', 75),
('Z100', 75),
('Z300', 75),
('Z360', 75),
('Z500', 75),
('Z560', 75),
('Z700', 75),
('Z800', 75),
('Z01', 75);

-- Foton
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Tunland', 76),
('View', 76),
('Forland', 76),
('Sauvana', 76),
('Thunder', 76),
('Daimler Auman', 76),
('Aumark', 76),
('Gratour', 76),
('SUP', 76),
('Toano', 76),
('Foton 2049', 76),
('Foton 1049', 76),
('Foton 1049L', 76),
('Foton 1069', 76),
('Foton 2049', 76),
('Foton 1029', 76),
('Foton 1069', 76),
('Foton 1029', 76),
('Foton 1049L', 76),
('Foton 2049', 76);

-- JAC
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('S2', 77),
('S3', 77),
('S4', 77),
('S5', 77),
('S7', 77),
('S8', 77),
('iEV7S', 77),
('iEV7', 77),
('iEV6E', 77),
('iEV6S', 77),
('iEV6', 77),
('iEV5', 77),
('iEV5S', 77),
('iEV6E', 77),
('iEV6S', 77),
('iEV6', 77),
('iEV5', 77),
('iEV5S', 77),
('iEV4', 77),
('iEV3', 77);

-- Changan
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('CS35 Plus', 78),
('CS55 Plus', 78),
('CS75 Plus', 78),
('CS85 COUPE', 78),
('Raeton CC', 78),
('CS95', 78),
('CS95A', 78),
('CS35', 78),
('CS55', 78),
('CS75', 78),
('CS85', 78),
('Raeton', 78),
('Raeton CC', 78),
('Raeton CC', 78),
('Raeton CC', 78),
('Alsvin', 78),
('Eado', 78),
('Eado XT', 78),
('Alsvin V7', 78),
('Eado EV460', 78);

-- Brilliance
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('V3', 79),
('V3 CROSS', 79),
('V5', 79),
('H3', 79),
('H3 CROSS', 79),
('H2', 79),
('H330', 79),
('H530', 79),
('H230', 79),
('H220', 79),
('H320', 79),
('FRV', 79),
('V5', 79),
('F20', 79),
('F40', 79),
('H530', 79),
('H320', 79),
('H220', 79),
('H230', 79),
('H330', 79);

-- Karry
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Youya', 80),
('Q12', 80),
('Youyou', 80),
('Q22', 80),
('K50', 80),
('Q21', 80),
('Q31', 80),
('Q32', 80),
('K60', 80),
('K50', 80),
('Q22', 80),
('K60', 80),
('Youyi', 80),
('Youme', 80),
('Youya', 80),
('Q12', 80),
('Youyou', 80),
('Q22', 80),
('K50', 80),
('Q21', 80);

-- Lifan
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('X60', 81),
('X70', 81),
('X80', 81),
('X90', 81),
('320', 81),
('330', 81),
('520', 81),
('530', 81),
('620', 81),
('630', 81),
('720', 81),
('820', 81),
('Breez', 81),
('Solano', 81),
('Smily', 81),
('Celliya', 81),
('X50', 81),
('X60', 81),
('X70', 81),
('X80', 81);

-- Haima
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('S5', 82),
('7X', 82),
('M3', 82),
('V70', 82),
('M6', 82),
('M8', 82),
('M5', 82),
('M4', 82),
('S7', 82),
('3', 82),
('2', 82),
('1', 82),
('V70', 82),
('S5', 82),
('S5', 82),
('S5', 82),
('7X', 82),
('7X', 82),
('7X', 82),
('S5', 82);

-- Changhe
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Ideal', 83),
('M50', 83),
('M70', 83),
('M80', 83),
('M90', 83),
('M100', 83),
('Beidouxing', 83),
('Liebao', 83),
('Freedom M50', 83),
('Freedom M70', 83),
('Freedom M80', 83),
('Freedom M90', 83),
('Freedom M100', 83),
('Liebao CS6', 83),
('Liebao CS7', 83),
('Liebao CS9', 83),
('Beidouxing M30', 83),
('Beidouxing M50', 83),
('Beidouxing M70', 83),
('Beidouxing M80', 83);

-- Wuling
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Hongguang', 84),
('Rongguang', 84),
('Sunshine', 84),
('Zhiguang', 84),
('Zhiguang PLUS', 84),
('Hongguang MINI EV', 84),
('Hongguang S1', 84),
('Hongguang S3', 84),
('Hongguang S5', 84),
('Hongguang S', 84),
('Hongguang V', 84),
('Hongguang X', 84),
('Wuling EV', 84),
('Zhiguang Plus', 84),
('Rongguang', 84),
('Sunshine', 84),
('Zhiguang', 84),
('Zhiguang PLUS', 84),
('Hongguang MINI EV', 84),
('Hongguang S1', 84);

-- Soueast
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('DX3', 85),
('DX5', 85),
('DX7', 85),
('V3', 85),
('V6', 85),
('V5', 85),
('V7', 85),
('V2', 85),
('V1', 85),
('DX3 EV', 85),
('DX5 EV', 85),
('V3 Lingyue EV', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85),
('V3 Lingyue', 85);

-- Landwind
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('X7', 86),
('X5', 86),
('X6', 86),
('X9', 86),
('X8', 86),
('X2', 86),
('X1', 86),
('X4', 86),
('X3', 86),
('X8', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86),
('X7', 86);

-- Yema
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('F99', 87),
('T70', 87),
('E70', 87),
('T60', 87),
('M70', 87),
('F99', 87),
('F10', 87),
('T70', 87),
('E70', 87),
('T60', 87),
('M70', 87),
('F99', 87),
('F10', 87),
('T70', 87),
('E70', 87),
('T60', 87),
('M70', 87),
('F99', 87),
('F10', 87),
('T70', 87);

-- ZNA
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Rich', 88),
('Ruiqi', 88),
('Ruiyi', 88),
('Junjie', 88),
('Ruiyi', 88),
('Noble', 88),
('Noble', 88),
('M1', 88),
('M2', 88),
('M3', 88),
('C1', 88),
('C2', 88),
('Z-Shine', 88),
('Ruiqi', 88),
('Ruiqi', 88),
('Ruiyi', 88),
('Junjie', 88),
('Ruiyi', 88),
('Noble', 88),
('Noble', 88);

-- JMC
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Yusheng', 89),
('Boarding', 89),
('Kairui', 89),
('Teshun', 89),
('Shunda', 89),
('Yuhu', 89),
('Teshun', 89),
('Shunda', 89),
('Yuhu', 89),
('Jinggangshan', 89),
('Yusheng', 89),
('Boarding', 89),
('Kairui', 89),
('Teshun', 89),
('Shunda', 89),
('Yuhu', 89),
('Teshun', 89),
('Shunda', 89),
('Yuhu', 89),
('Jinggangshan', 89);

-- Leyland
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('DOST', 90),
('Stile', 90),
('MiTR', 90),
('Partner', 90),
('Haulage', 90),
('Comet', 90),
('DOST+', 90),
('DOST CNG', 90),
('DOST LCV', 90),
('Stile LE', 90),
('Stile LS', 90),
('MiTR CNG', 90),
('MiTR 109', 90),
('MiTR 1214', 90),
('MiTR 1214 CRDi', 90),
('MiTR 1618', 90),
('MiTR 1618 CRDi', 90),
('MiTR 1618 CNG', 90),
('Partner', 90),
('Partner N-Dura', 90);

-- Tata
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Nexon', 91),
('Tiago', 91),
('Tigor', 91),
('Altroz', 91),
('Harrier', 91),
('Safari', 91),
('Punch', 91),
('Hexa', 91),
('Sumo', 91),
('Safari Storme', 91),
('Safari Dicor', 91),
('Indica', 91),
('Indigo', 91),
('Nano', 91),
('Zest', 91),
('Bolt', 91),
('Aria', 91),
('Vista', 91),
('Safari Dicor', 91),
('Grande', 91);

-- Mahindra
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Scorpio', 92),
('Thar', 92),
('XUV500', 92),
('Bolero', 92),
('XUV300', 92),
('KUV100', 92),
('TUV300', 92),
('Alturas G4', 92),
('Marazzo', 92),
('Verito', 92),
('e-Verito', 92),
('Xylo', 92),
('NuvoSport', 92),
('Quanto', 92),
('Jeeto', 92),
('Supro', 92),
('Maxximo', 92),
('Marshal', 92),
('XUV Aero', 92),
('Inferno', 92);

-- Ashok Leyland
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('DOST', 93),
('Stile', 93),
('MiTR', 93),
('Partner', 93),
('Haulage', 93),
('Comet', 93),
('DOST+', 93),
('DOST CNG', 93),
('DOST LCV', 93),
('Stile LE', 93),
('Stile LS', 93),
('MiTR CNG', 93),
('MiTR 109', 93),
('MiTR 1214', 93),
('MiTR 1214 CRDi', 93),
('MiTR 1618', 93),
('MiTR 1618 CRDi', 93),
('MiTR 1618 CNG', 93),
('Partner', 93),
('Partner N-Dura', 93);

-- Maruti
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Swift', 94),
('Dzire', 94),
('Baleno', 94),
('Alto', 94),
('Wagon R', 94),
('Vitara Brezza', 94),
('Ertiga', 94),
('Ciaz', 94),
('S-Presso', 94),
('Ignis', 94),
('S-Cross', 94),
('Celerio', 94),
('XL6', 94),
('Celerio X', 94),
('Eeco', 94),
('Omni', 94),
('Gypsy', 94),
('Grand Vitara', 94),
('Versa', 94),
('800', 94);

-- Force Motors
INSERT INTO vehicle_models (name, vehicle_brand_id) VALUES 
('Traveller', 95),
('Gurkha', 95),
('Trax', 95),
('Traveller 26', 95),
('Traveller 26 School Bus', 95),
('Traveller 26 Staff Bus', 95),
('Traveller 26 Tourism', 95),
('Traveller 26 Ambulance', 95),
('Traveller 26 Delivery Van', 95),
('Traveller 26 Goods Carrier', 95),
('Traveller 26 Mini Bus', 95),
('Traveller 26 Ambulance', 95),
('Traveller 26 Delivery Van', 95),
('Traveller 26 Goods Carrier', 95),
('Traveller 26 Mini Bus', 95),
('Trax Cruiser', 95),
('Trax Cruiser Ambulance', 95),
('Trax Cruiser School Bus', 95),
('Trax Cruiser Staff Bus', 95),
('Trax Cruiser Tourism', 95);


CREATE TABLE vehicles (
    id INT AUTO_INCREMENT NOT NULL,
    vehicle_model_id INT DEFAULT NULL,
    vehicle_year INT DEFAULT NULL,
    color VARCHAR(55) NOT NULL,
    vehicle_type_id INT DEFAULT NULL,
    customer_id INT NOT NULL,
    vehicle_registration VARCHAR(50),
    kilometer_accounting INT DEFAULT 0,

    PRIMARY KEY (id),
    FOREIGN KEY (vehicle_model_id) REFERENCES vehicle_models(id),
    FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_types(id),
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    KEY (vehicle_registration)
);

CREATE TABLE working_order_status (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,

    PRIMARY KEY (id)
);

INSERT INTO working_order_status (name) VALUES 
('Trabajando'),
('Finalizado');

CREATE TABLE working_orders (
    id INT AUTO_INCREMENT NOT NULL,
    vehicle_id INT NOT NULL,
    user_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ended_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    problem_description TEXT DEFAULT NULL,
    working_observations TEXT DEFAULT NULL,
    working_order_status_id INT DEFAULT 1,

    PRIMARY KEY (id),
    FOREIGN KEY (vehicle_id) REFERENCES vehicles(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (working_order_status_id) REFERENCES working_order_status(id)
);

CREATE TABLE spareparts (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    description TEXT DEFAULT NULL,
    last_cost INT DEFAULT 0,
    sku VARCHAR(150) DEFAULT NULL,

    PRIMARY KEY (id)
);

/* insert spareparts */

CREATE TABLE working_orders_spareparts (
    working_order_id INT,
    sparepart_id INT,
    quantity INT,
    price INT,

    PRIMARY KEY (working_order_id, sparepart_id),
    FOREIGN KEY (working_order_id) REFERENCES working_orders(id),
    FOREIGN KEY (sparepart_id) REFERENCES spareparts(id)
);

CREATE TABLE services (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(150) NOT NULL,
    description TEXT DEFAULT NULL,

    PRIMARY KEY (id)
);

/* insert services */
INSERT INTO services (name, description) 
    VALUES 
    ('Cambio bulbo freno', 'Cambio bulbo freno'),
    ('Cambio pastillas delantero', 'Cambio pastillas tren delantero'),
    ('Cambio pastillas trasero', 'Cambio pastillas tren trasero'),
    ('Cambio piola', 'Cambio piola'),
    ('Cambio servo', 'Cambio servo'),
    ('Regulación de freno', 'Regulación de frenos sin repuestos'),
    ('Rectificado discos delanteros', 'Rectificados discos tren delantero'),
    ('Rectificado discos trasero', 'Rectificados discos tren trasero'),
    ('Rectificado tambor delantero', 'Rectificados tambor tren trasero'),
    ('Reparación caliper', 'Reparación caliper'),
    ('Reparación cilindro', 'Reparación cilindros'),
    ('Reparación servo', 'Reparación servo'),
    ('Servicios varios', 'Servicios varios');

CREATE TABLE working_orders_services (
    working_order_id INT,
    service_id INT,
    quantity INT,
    price INT,

    PRIMARY KEY (working_order_id, service_id),
    FOREIGN KEY (working_order_id) REFERENCES working_orders(id),
    FOREIGN KEY (service_id) REFERENCES services(id)
);


-- Creación de data inicial
INSERT INTO addresses (address, city_id) 
    VALUES 
    ('Avenida mexico 4363', 42);

INSERT INTO customers (name, address_id, phone, email) 
    VALUES 
    ('Josimar Gonzalez', 3, '945636229', 'josimar.gonzalez@gmail.com');

INSERT INTO vehicles (vehicle_model_id, vehicle_year, color, vehicle_type_id, customer_id, vehicle_registration, kilometer_accounting) 
    VALUES 
    (304, 2007, 'Gris', 3, 1, '', 200000);

INSERT INTO working_orders (vehicle_id, user_id, problem_description, working_observations)
    VALUES
    (1, 6, 'Problema de frenado y vibración al frenar', 'Se realiza cambio de pastillas, se hace mantención a caliper y nse rectifican discos');

INSERT INTO working_orders_services (working_order_id, service_id, quantity, price)
    VALUES
    (1, 2, 1, 30000),
    (1, 3, 1, 40000),
    (1, 7, 2, 15000);


