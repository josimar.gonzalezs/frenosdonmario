-- Creación de la base de datos
CREATE DATABASE taller_mecanico;

-- Selección de la base de datos
USE taller_mecanico;

-- Creación de la tabla Cliente
CREATE TABLE Cliente (
    ID_Cliente INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Dirección VARCHAR(100),
    Teléfono VARCHAR(20),
    CorreoElectrónico VARCHAR(100)
);

-- Creación de la tabla Vehículo
CREATE TABLE Vehículo (
    ID_Vehículo INT AUTO_INCREMENT PRIMARY KEY,
    Cliente_ID INT,
    Marca VARCHAR(50),
    Modelo VARCHAR(50),
    Año INT,
    NúmeroDeChasis VARCHAR(50),
    Kilometraje INT,
    FOREIGN KEY (Cliente_ID) REFERENCES Cliente(ID_Cliente)
);

-- Creación de la tabla Orden de Trabajo
CREATE TABLE OrdenDeTrabajo (
    ID_Orden INT AUTO_INCREMENT PRIMARY KEY,
    Vehículo_ID INT,
    FechaEntrada DATE,
    FechaSalida DATE,
    DescripciónProblema TEXT,
    Observaciones TEXT,
    EstadoOrden VARCHAR(50),
    FOREIGN KEY (Vehículo_ID) REFERENCES Vehículo(ID_Vehículo)
);

-- Creación de la tabla Empleado
CREATE TABLE Empleado (
    ID_Empleado INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Dirección VARCHAR(100),
    Teléfono VARCHAR(20),
    CorreoElectrónico VARCHAR(100),
    Rol VARCHAR(50)
);

-- Creación de la tabla Repuesto
CREATE TABLE Repuesto (
    ID_Repuesto INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(50),
    Descripción TEXT,
    PrecioUnitario DECIMAL(10, 2)
);

-- Creación de la tabla OrdenDeTrabajo_Repuesto para la relación muchos a muchos
CREATE TABLE OrdenDeTrabajo_Repuesto (
    Orden_ID INT,
    Repuesto_ID INT,
    PRIMARY KEY (Orden_ID, Repuesto_ID),
    FOREIGN KEY (Orden_ID) REFERENCES OrdenDeTrabajo(ID_Orden),
    FOREIGN KEY (Repuesto_ID) REFERENCES Repuesto(ID_Repuesto)
);
